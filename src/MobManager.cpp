#include "MobManager.h"
#include "Server.h"
#include "Item.h"
#include "XMLReader.h"
#include <random>

std::map<string, MobClass*> MobManager::ListOfMobs;
std::vector<MobClass*> MobManager::IntListOfMobs;
std::map<string, vector<int>> MobManager::BiomeMobMap;

bool MobManager::LoadMobs(string file, string type)
{
	// create an XML file reader
	XMLReader XML(file);
	if (!XML.IsOpen())
		return false;

	// read the file
	const string strMobsTag("mobs");
	string strCurrentSection, strNodeName;
	MobClass *mob = NULL;
	bool bLoaded = false;

	while (XML.ReadLine())
	{
		if (XML.IsNode(strMobsTag))
			strCurrentSection = strMobsTag;
		else if (strCurrentSection == strMobsTag && XML.IsNode(type))
			strCurrentSection = type;
		else if (strCurrentSection == type)
		{
			// check if we have reached the end of the item
			if (XML.IsEndOfNode(strCurrentSection))
			{
				strCurrentSection = strMobsTag;

				continue;
			}

			// look for what we need
			strNodeName = XML.GetNodeName();
			if (strNodeName == "name")
			{
				mob = new MobClass();
				mob->strName = XML.GetAttributeValue("value");

				// make sure this item doesn't already exist
				if (ListOfMobs[mob->strName] != NULL)
				{
					delete mob;
					mob = NULL;

					strCurrentSection = strMobsTag;
				}
				else
				{
					ListOfMobs[mob->strName] = mob;
					bLoaded = true; // we have loaded at least one so return true
					mob->index = IntListOfMobs.size();
					IntListOfMobs.push_back(mob);
				}
			}
			else if (strNodeName == "damage")
			{
				mob->sDamage = atoi(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "health")
			{
				mob->iHealth = atoi(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "range")
			{
				mob->fRange = (float)atof(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "biome")
			{
				string biome = XML.GetAttributeValue("value");

				// check to see if there's an array for this biome already
				vector<int>* mobs = NULL;
				if (BiomeMobMap.find(biome) == BiomeMobMap.end())
				{
					BiomeMobMap.emplace(biome, vector<int>());
				}

				mobs = &BiomeMobMap[biome];

				// add our new mob to the biome mob map
				mobs->push_back(mob->index);
			}
			else if (strNodeName == "class")
			{
				string style = XML.GetAttributeValue("value");
				if (style == "ranged")
					mob->Style = ranged;
			}
			else if (strNodeName == "projectile")
			{
				const ItemClass* ic = Item::GetItemClass(XML.GetAttributeValue("name"));
				mob->iProjectileIndex = ic->index;
			}
		}
	}

	return bLoaded;
}

void MobManager::UnloadMobs()
{
	for (std::map<string, MobClass*>::iterator it = ListOfMobs.begin(); it != ListOfMobs.end(); ++it) {
		delete it->second;
	}
	ListOfMobs.clear();
	IntListOfMobs.clear();
	BiomeMobMap.clear();
}

bool MobManager::ReloadMobs(string file, string type)
{
	UnloadMobs();
	return LoadMobs(file, type);
}

const MobClass* MobManager::GetMobClass(string name)
{
	return ListOfMobs[name];
}

const MobClass* MobManager::GetMobClass(int id)
{
	return IntListOfMobs[id];
}

MobManager::MobManager(Server* server, TerrainManager* terrain)
	: m_pServer(server)
	, m_pTerrain(terrain)
{
	// get the maximum amount of mobs
	int maxmobs = m_pServer->GetMaxClientCount();
	m_MaxMobs = maxmobs * MAX_MOBS_PER_CLIENT;
	
	// ensure the mob array is clear
	m_Mobs.reserve(maxmobs);
	for (int i = 0; i < m_MaxMobs; i++) {
		m_Mobs.push_back(NULL);
	}

	// add values to the mobs per client map
	for (int i = 0; i < maxmobs; i++) {
		m_MobsPerClient.emplace(i, vector<Mob*>());
	}
}

MobManager::~MobManager()
{
	// delete any alive mobs
	for (GLuint i = 0; i < m_MaxMobs; i++) {
		if (m_Mobs[i])
		{
			delete m_Mobs[i];
			m_Mobs[i] = NULL;
		}
	}
}

void MobManager::ThreadTask(MobManager* m)
{
	// call update
	m->Update();
}

Client* FindClientById(vector<Client*>& list, int id)
{
	// helper function to check if the client with the given id exists in the list
	for (GLuint i = 0; i < list.size(); i++) {
		if (list.at(i)->clientid == id)
			return list.at(i);
	}

	return NULL;
}

void MobManager::Update()
{
	// sizeof MobData defined here
	const size_t MobDataSize = sizeof(MobData);
	const int maxClientCount = m_pServer->GetMaxClientCount();
	unsigned char* buffer = new unsigned char[BUFFER_SIZE];
	
	// main loop
	while (m_pServer->IsRunning())
	{
		// set the seed for random events
		srand(std::chrono::system_clock::now().time_since_epoch().count());
		
		// get a list of connected clients
		vector<Client*> clients;
		if (!m_pServer->GetListOfClients(clients))
		{
			// delete any currently active mobs
			for (GLuint i = 0; i < m_MaxMobs; i++) {
				if (m_Mobs[i])
				{
					delete m_Mobs[i];
					m_Mobs[i] = NULL;
				}
			}

			// clear the mobs per client
			map<int, vector<Mob*>>::iterator it = m_MobsPerClient.begin();
			while (it != m_MobsPerClient.end())
			{
				it->second.clear();

				it++;
			}

			// we can skip the rest of the code so sleep 250 milliseconds and try again
			std::this_thread::sleep_for(std::chrono::milliseconds(250));
			continue;
		}

		// get the whole list of clients to make finding clients by id quicker
		Client** allclients = m_pServer->GetWholeListOfClients();
		
		// deal with the creation of mobs
		for (GLuint i = 0; i < maxClientCount; i++) {
			// check if this client is still connected to the server
			Client* client = allclients[i];
			int count = m_MobsPerClient.at(i).size();
			if (client)
			{
				int index = GetFreeMob();
				if (index > -1 && count < MAX_MOBS_PER_CLIENT)
				{
					// there's a chance a mob will spawn
					if (rand() % 1000 < 10) // disabled to test new water
					{
						// spawn a mob near this client!
						Mob* mob = new Mob;

						// choose a spawn point and create a mob based on the biome at that location
						vector3df pos;
						TerrainChunk* pChunk;
						vector3di tmp1, tmp2;
						GLfloat dir;
						bool bSpotFound = false;
						int count = 0;

						do {
							dir = (rand() % 360) * DEG_TO_RAD;
							pos = client->data.pos + vector3df(cos(dir) * 300.f, 160.f, sin(dir) * 300.f);//vector3df(rand() % 500 - rand() % 1000, 160, rand() % 500 - rand() % 1000);

							bSpotFound = m_pTerrain->GetPositionInChunk(pos, pChunk, tmp1, tmp2);
							++count;
						}
						while (!bSpotFound && count < 20);

						if (bSpotFound)
						{
							vector<int>* mobs = MobList(m_pTerrain->GetBiome(pos).name);

							// create the mob
							if (mobs && !mobs->empty())
							{
								// give the mob a pointer to the mobclass we're spawning
								int id = mobs->at(rand() % mobs->size());
								mob->type = IntListOfMobs[id];

								// initial values
								mob->target = i;
								mob->grav = 0;
								mob->zerohealthcount = 5;
								mob->hitcount = 0;
								mob->rangecount = 0;
								mob->lunge = false;

								// we'll choose the mob based on biome, height, time of day, world difficulty...
								mob->data.id = id;
								mob->data.health = mob->type->iHealth + rand() % (mob->type->iHealth / 4);
								mob->data.maxhealth = mob->data.health;
								mob->data.pos = pos;
								mob->data.pos.y = m_pTerrain->GetHeightBelow(pos);

								// put this in the array of mobs
								mob->data.index = index;
								m_Mobs[index] = mob;
								m_MobsPerClient.at(i).push_back(mob);

								_out << "New mob!" << _end;
							}
						}
					}
				}
			}
			else
			{
				if (count > 0)
				{
					// set the mob count for this client to 0
					m_MobsPerClient.at(i).clear();

					// go through each mob, if they are near to another client then target them, otherwise destroy them
					for (GLuint j = 0; j < m_MaxMobs; j++) {
						if (m_Mobs[j] && m_Mobs[j]->target == i)
						{
							// atempt to reassign to another client
							ReassignMob(j, clients);
						}
					}
				}
			}
		}

		// deal with updating each individual mob
		for (GLuint i = 0; i < m_MaxMobs; i++) {
			if (m_Mobs[i])
			{
				// if this mob is dead, count down to its deletion
				if (m_Mobs[i]->data.health <= 0)
				{
					if (--m_Mobs[i]->zerohealthcount == 0)
					{
						// erase us from our current target's list
						vector<Mob*>::iterator it = std::find(m_MobsPerClient.at(m_Mobs[i]->target).begin(), m_MobsPerClient.at(m_Mobs[i]->target).end(), m_Mobs[i]);
						if (it != m_MobsPerClient.at(m_Mobs[i]->target).end())
							m_MobsPerClient.at(m_Mobs[i]->target).erase(it);

						delete m_Mobs[i];
						m_Mobs[i] = NULL;
					}
					continue;
				}

				// hit count
				if (m_Mobs[i]->hitcount > 0)
					m_Mobs[i]->hitcount--;

				// ranged attack count
				if (m_Mobs[i]->rangecount > 0)
					m_Mobs[i]->rangecount--;
				
				// get our target
				Client* client = allclients[m_Mobs[i]->target];

				if (client)
				{
					// if our target is dead, don't do anything
					if (client->stats.health == 0)
					{
						// set the attacking bit to 0
						SetBit(m_Mobs[i]->data.flags, 0, 0);

						// move us
						MoveMob(m_Mobs[i]);
					}
					else
					{
						// chase after our target
						GLfloat dist = client->data.pos.QuickDistance(m_Mobs[i]->data.pos);

						// if we are too far away from our target, attempt to reassign us to another player, or just delete us
						if (dist > 160000.f && !ReassignMob(i, clients))
						{
							_out << "Mob out of range" << _end;
							continue;
						}
						else
							client = allclients[m_Mobs[i]->target];

						// if we have a new client and they don't exist (no idea HOW this would happen) then continue
						if (!client)
						{
							_out << "Something REALLY bad has happened, please report this!" << _end;
							continue;
						}

						// look towards the target
						vector3df diff = client->data.pos - m_Mobs[i]->data.pos;
						m_Mobs[i]->dir = atan2(diff.z, diff.x);
						m_Mobs[i]->data.rot = m_Mobs[i]->dir;

						// set the attacking bit to 0
						SetBit(m_Mobs[i]->data.flags, 0, 0);

						if (m_Mobs[i]->knockbackspeed > 0)
						{
							// knock back this mob
							vector3df check(m_Mobs[i]->knockbackspeed * cos(m_Mobs[i]->knockbackdir), 0, m_Mobs[i]->knockbackspeed * sin(m_Mobs[i]->knockbackdir));

							// x collision
							GLfloat height = m_pTerrain->GetHeightBelow(m_Mobs[i]->data.pos + vector3df(check.x, 5, 0));
							if (height - m_Mobs[i]->data.pos.y > 5.f)
								check.x = 0;

							// z collision
							height = m_pTerrain->GetHeightBelow(m_Mobs[i]->data.pos + vector3df(0, 5, check.z));
							if (height - m_Mobs[i]->data.pos.y > 5.f)
								check.z = 0;

							m_Mobs[i]->data.pos += check;
							m_Mobs[i]->knockbackspeed -= 1.75f;
						}
						else
						{
							m_Mobs[i]->lunge = false;
						}
						
						if (m_Mobs[i]->knockbackspeed <= 0.f || m_Mobs[i]->lunge)
						{
							// move us
							if (m_Mobs[i]->type->Style == melee || dist > m_Mobs[i]->type->fRange)
								MoveMob(m_Mobs[i]);

							// lunge at a player if melee
							if (m_Mobs[i]->knockbackspeed <= 0.f && m_Mobs[i]->type->Style == melee && dist < m_Mobs[i]->type->fRange + 500.f && rand() % 100 < 9)
							{
								vector3df diff = client->data.pos - m_Mobs[i]->data.pos;
								GLfloat knockbackdir = atan2(diff.z, diff.x);

								KnockBack(m_Mobs[i], knockbackdir, 7.f);
								m_Mobs[i]->lunge = true;
							}

							if (dist < m_Mobs[i]->type->fRange + 30.f)
							{
								// set the attacking bit to 1
								SetBit(m_Mobs[i]->data.flags, 0, 1);

								// hit the target
								if (dist < m_Mobs[i]->type->fRange)
								{
									if (m_Mobs[i]->type->Style == melee)
									{
										// this is a melee mob, hit the player
										m_pServer->HitClient(m_Mobs[i]->target, m_Mobs[i]->type->sDamage, m_Mobs[i]->data.pos);
									}
									else
									{
										// this is a ranged mob, see if we can fire our ranged attack
										if (m_Mobs[i]->rangecount <= 0)
										{
											const ItemClass* projectile = Item::GetItemClass(m_Mobs[i]->type->iProjectileIndex);
											m_pServer->CreateProjectile(projectile, m_Mobs[i]->data.pos + vector3df(0, 10, 0), client->data.pos + vector3df(0, 4, 0), m_Mobs[i]->type->sDamage, 2);

											m_Mobs[i]->rangecount = 20;
										}
									}
								}
							}
						}
					}

					// get the ground position below this mob
					GLfloat ground = m_pTerrain->GetHeightBelow(m_Mobs[i]->data.pos + vector3df(0, 5, 0));
					GLfloat ceiling = m_pTerrain->GetDistanceAbove(m_Mobs[i]->data.pos + vector3df(0, 15, 0));

					// stop jumping up through floors
					if (ceiling <= 3.0f)
					{
						if (m_Mobs[i]->grav > 0.f)
							m_Mobs[i]->grav = 0.f;
					}

					// gravity
					if (m_Mobs[i]->data.pos.y > ground || m_Mobs[i]->grav > 0)
					{
						m_Mobs[i]->data.pos.y += m_Mobs[i]->grav;
						m_Mobs[i]->grav -= 1.5f;
					}
					
					// stop the mob from falling through the ground
					if (m_Mobs[i]->data.pos.y <= ground && ceiling > 3.0f)
					{
						m_Mobs[i]->data.pos.y = ground;
						m_Mobs[i]->grav = 0;
					}
				}
				else
				{
					// the client doesn't exist on the server anymore? we shouldn't really reach this here...
					_out << "We have hit a case we were never meant to hit..." << _end;
				}
			}
		}

		// loop through each client and send them mob data for mobs near them
		for (GLuint i = 0; i < clients.size(); i++) {
			vector<unsigned char> data;

			// mobs will be added to the data vector if they are within 300 meters of the client
			for (int j = 0; j < m_MaxMobs; j++) {
				if (m_Mobs[j] && clients[i]->data.pos.QuickDistance(m_Mobs[j]->data.pos) <= 50000.f)
				{
					data.reserve(MobDataSize);
					unsigned char* buf = MobDataToBuffer(m_Mobs[j]->data);

					for (GLuint u = 0; u < MobDataSize; u++) {
						data.push_back(buf[u]);
					}
				}
			}

			if (!data.empty())
			{
				// compress the data
				uLongf size = BUFFER_SIZE;
				memset(buffer, 0, BUFFER_SIZE);
				compress(buffer, &size, &data[0], data.size());

				// send the data to the client
				m_pServer->SendPacket(clients[i]->clientid, mob_updates, buffer, size);
			}
		}

		// sleep for 100 milliseconds
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	// clean up
	delete buffer;
}

int MobManager::GetFreeMob()
{
	// find the next free mob slot
	for (int i = 0; i < m_MaxMobs; i++) {
		if (!m_Mobs[i])
			return i;
	}

	return -1;
}

void MobManager::ClientMeleeAttacking(Client* client)
{
	// a client is swinging
	const ItemClass* ic = client->hotbar.items.at(client->data.selected).ic;

	// we definitely know the client is swinging a weapon, let's try and hit a mob
	for (int i = 0; i < m_MaxMobs; i++) {
		if (m_Mobs[i] && m_Mobs[i]->data.health > 0)
		{
			// check if this player has hit this mob
			if (client->data.pos.QuickDistance(m_Mobs[i]->data.pos) <= 175.f && m_Mobs[i]->hitcount <= 0)
			{
				// is this client looking at this mob?
				vector3df diff = client->data.pos - m_Mobs[i]->data.pos;
				float angle = atan2(diff.z, diff.x);
				float clientdir = -client->data.rot.y + PI;

				// calculate look direction in relation to direction between mob and player
				bool lookingat = (abs(angle - clientdir) < PI_OVER_4 || abs(angle + TWO_PI - clientdir) < PI_OVER_4 || abs(angle - TWO_PI - clientdir) < PI_OVER_4);
				
				if (lookingat)
				{
					// dodge/blocking chance - if the mob has been created with the ability to dodge/block attacks, let's give them a chance to do so here
					// TEMPORARY: until i add the options to the xml file (which will take like 2 minutes but you know), always have a chance to dodge
					if (!Dodge(m_Mobs[i], angle))
					{
						m_Mobs[i]->data.health -= ic->sValues[1] + rand() % (ic->sValues[1] / 3);
						m_Mobs[i]->hitcount = 5;
						KnockBack(i, angle + PI, 10.f * ((float)ic->sValues[6] / 100.f));

						// if this is a ranged mob, stop them from shooting for a tiny bit
						if (m_Mobs[i]->type->Style == ranged)
							m_Mobs[i]->rangecount += 5;

						// if we aren't targetting this client target them now
						if (m_Mobs[i]->target != client->clientid)
							m_Mobs[i]->target = client->clientid;
					}
				}
#ifdef _DEBUG
				else
				{
					// print the angles so i can see wtf is going on
					_out << angle << " - " << clientdir << _end;
				}
#endif
			}
		}
	}
}

void MobManager::KnockBack(int index, float dir, float speed)
{
	// knock back this mob
	KnockBack(m_Mobs[index], dir, speed);
}

void MobManager::KnockBack(Mob* mob, float dir, float speed)
{
	// knock back this mob
	mob->knockbackdir = dir;
	mob->knockbackspeed = speed;
	mob->grav = 2;
}

const vector<Mob*>* MobManager::GetMobs() const
{
	return &m_Mobs;
}

bool MobManager::Dodge(Mob* mob, float angle)
{
	// dodge/blocking chance - if the mob has been created with the ability to dodge/block attacks, let's give them a chance to do so here
	// TEMPORARY: until i add the options to the xml file (which will take like 2 minutes but you know), always have a chance to dodge
	int dodgeChance = rand() % 100;
	if (mob->knockbackspeed <= 0.1f && dodgeChance < 60)
	{
		int flipChance = rand() % 3;
		int flip = (flipChance == 1 ? 1 : (flipChance == 2 ? 2 : -1));
		KnockBack(mob, angle + (PI_OVER_2 * flip), 7.f); // use the knockback function to dodge sideways

		return true;
	}

	return false;
}

void MobManager::MoveMob(Mob* mob)
{
	// get the ground position below this mob
	GLfloat ground = m_pTerrain->GetHeightBelow(mob->data.pos + vector3df(0, 5, 0));
	
	// check for terrain collision and jumping
	vector3df pos = mob->data.pos;
	vector3df check(2.f * cos(mob->dir), 0, 2.f * sin(mob->dir));

	// x collision
	/*GLfloat height = m_pTerrain->GetHeightBelow(pos + vector3df(check.x, 5, 0));
	if (height - pos.y > 5.f)
	{
		// try jumping
		if (mob->data.pos.y <= ground + 0.5f && mob->grav == 0)
			mob->grav = 5;

		// if not, don't move this way
		check.x = 0;
	}

	// z collision
	height = m_pTerrain->GetHeightBelow(pos + vector3df(0, 5, check.z));
	if (height - pos.y > 5.f)
	{
		// try jumping
		if (mob->data.pos.y <= ground + 0.5f && mob->grav == 0)
			mob->grav = 5;

		// if not, don't move this way
		check.z = 0;
	}*/
	if (MoveCheck(mob->data.pos, check.x, check.z) && mob->grav == 0)
		mob->grav = 5;

	// now check for any collisions with mobs with the same target
	vector<Mob*>* mobs = &m_MobsPerClient.at(mob->target);
	if (mobs->size() > 1)
	{
		// there are other mobs targetting this client, check we don't walk into them
		for (GLuint i = 0; i < mobs->size() && (check.x != 0 || check.z != 0); i++) {
			Mob* other = mobs->at(i);
			if (mob->data.index != other->data.index && other->data.health > 0 && mob->data.pos.QuickDistance(other->data.pos) <= 20.f)
			{
				// check x
				if (other->data.pos.QuickDistance(mob->data.pos + vector3df(check.x, 0, 0)) <= 10.f)
					check.x = -check.x;

				// check z
				if (other->data.pos.QuickDistance(mob->data.pos + vector3df(0, 0, check.z)) <= 10.f)
					check.z = -check.z;
			}
		}
	}

	// move towards the target
	mob->data.pos += check;
}

vector<int>* MobManager::MobList(string biome)
{
	// get a pointer to the list of mobs that can spawn in the given biome
	if (BiomeMobMap.find(biome) == BiomeMobMap.end())
		return NULL;

	return &BiomeMobMap[biome];
}

bool MobManager::ReassignMob(GLuint mob, std::vector<Client*>& clients)
{
	// atempt to reassign to another client
	bool bReassigned = false;
	for (GLuint u = 0; u < clients.size(); u++) {
		if (clients[u]->data.pos.QuickDistance(m_Mobs[mob]->data.pos) <= 80000.f)
		{
			// erase us from our current target's list
			vector<Mob*>::iterator it = std::find(m_MobsPerClient.at(m_Mobs[mob]->target).begin(), m_MobsPerClient.at(m_Mobs[mob]->target).end(), m_Mobs[mob]);
			if (it != m_MobsPerClient.at(m_Mobs[mob]->target).end())
				m_MobsPerClient.at(m_Mobs[mob]->target).erase(it);

			m_Mobs[mob]->target = clients[u]->clientid;
			m_MobsPerClient.at(m_Mobs[mob]->target).push_back(m_Mobs[mob]);
			bReassigned = true;
			break;
		}
	}

	// if we were't able to reassign, delete the mob
	if (!bReassigned)
	{
		// erase us from our current target's list
		vector<Mob*>::iterator it = std::find(m_MobsPerClient.at(m_Mobs[mob]->target).begin(), m_MobsPerClient.at(m_Mobs[mob]->target).end(), m_Mobs[mob]);
		if (it != m_MobsPerClient.at(m_Mobs[mob]->target).end())
			m_MobsPerClient.at(m_Mobs[mob]->target).erase(it);

		delete m_Mobs[mob];
		m_Mobs[mob] = NULL;
	}

	return bReassigned;
}

bool MobManager::MoveCheck(const vector3df& pos, GLfloat& fMoveX, GLfloat& fMoveY)
{
	bool bReturn = false;
	float fCheckX = fMoveX;
	float fCheckY = fMoveY;

	if (fCheckX < 0)
		fCheckX = std::min(fCheckX, -2.0f);
	else
		fCheckX = std::max(fCheckX, 2.0f);

	if (fCheckY < 0)
		fCheckY = std::min(fCheckY, -2.0f);
	else
		fCheckY = std::max(fCheckY, 2.0f);

	vector3df check = pos;
	GLfloat aboveDist = 0.f;
	GLfloat moveHeight = check.y;
	check.y += 12.0f;

	// check the x movement
	check.x += fCheckX;
	moveHeight = m_pTerrain->GetHeightBelow(check);
	aboveDist = m_pTerrain->GetDistanceAbove(check);
	if (moveHeight - (pos.y - 6.0f) > 10.0f || aboveDist < 4.0f)
	{
		fMoveX = 0;

		if (pos.y <= moveHeight + 0.5f)
			bReturn = true;
	}

	// check the z movement
	check.z = pos.z;
	check.y = pos.y + 12.0f;
	check.x = pos.x;
	check.z += fCheckY;
	moveHeight = m_pTerrain->GetHeightBelow(check);
	aboveDist = m_pTerrain->GetDistanceAbove(check);
	if (moveHeight - (pos.y - 6.0f) > 10.0f || aboveDist < 4.0f)
	{
		fMoveY = 0;

		if (pos.y <= moveHeight + 0.5f)
			bReturn = true;
	}

	if (fMoveY != 0.f)
	{
		check.z = pos.z;
		check.y = pos.y + 12.0f;
		check.x = pos.x + fCheckX;
		check.z += fCheckY;
		moveHeight = m_pTerrain->GetHeightBelow(check);
		aboveDist = m_pTerrain->GetDistanceAbove(check);
		if (moveHeight - (pos.y - 6.0f) > 14.0f || abs(aboveDist) < 4.0f)
		{
			fMoveX = 0;

			if (pos.y <= moveHeight + 0.5f)
				bReturn = true;
		}
	}

	return bReturn;
}
