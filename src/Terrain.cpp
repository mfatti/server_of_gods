#include "Terrain.h"
#include "Server.h"
#include "zlib.h"
#include "Item.h"
#include "XMLReader.h"
#include <chrono>
#include <sstream>
#include <fstream>

bool TerrainManager::m_bSaving = false;
bool TerrainManager::m_bShuttingDown = false;

void CleanUpThread(TerrainManager* t);

TerrainManager::TerrainManager(Server* server, string name, long seed, string biomesfile)
	: m_Noise(seed)
	, m_TerrainSeed(seed)
	, m_pServer(server)
	, m_WorldName(name)
	, m_BiomesFile(biomesfile)
	, m_SectorSize(128)
	, m_ChunkLengthPerSector(64)
{
	// check that the save folder exists first!
	string saveDir = string("save/");
	if (!DirectoryExists(saveDir))
	{
#ifdef _WIN32
		_wmkdir(ToWideString(saveDir).c_str());
#else
		mkdir(saveDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#endif
	}
	
	// we have been given a world name, check if this is a new world or an existing one
	string worldDir = ToLower(saveDir.append(name));
	if (!DirectoryExists(worldDir))
	{
		// this is a new world! create the directory structure for this world
#ifdef _WIN32
		// windows version
		_wmkdir(ToWideString(worldDir).c_str());
		_wmkdir(ToWideString(string(worldDir).append("/chunkdata")).c_str());
		_wmkdir(ToWideString(string(worldDir).append("/chunkdetail")).c_str());
#else
		// linux version (well, anything NOT windows but this is only compiled on windows and linux so...)
		if (mkdir(worldDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0)
			std::cout << "Error creating directory " << worldDir << ", error is: " << errno << std::endl;
		mkdir(string(worldDir).append("/chunkdata").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		mkdir(string(worldDir).append("/chunkdetail").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#endif
	}
}

TerrainManager::~TerrainManager()
{
	// clear the terrain memory
	TerrainMap::iterator it = m_Terrain.begin();
	while (it != m_Terrain.end())
	{
		// delete the chunk
		DeleteChunk(it->second);

		it++;
	}
	m_Terrain.clear();

	std::map<vector2di, std::vector<GLfloat>>::iterator nit = m_TerrainNoise.begin();
	while (nit != m_TerrainNoise.end())
	{
		nit->second.clear();

		nit++;
	}
	m_TerrainNoise.clear();
}

void CleanUpThread(TerrainManager* t)
{
	while (true)
	{
		// sleep for two minutes
		std::this_thread::sleep_for(std::chrono::seconds(20));

		// alert us that we are cleaning up
		_out << "Cleaning up free chunks..." << _end;

		// call the clean up function to free memory / cpu load
		t->CleanUpVacantChunks();
	}
}

void TerrainManager::DeleteChunk(TerrainChunk* chunk)
{
	// delete multi block lists
	map<vector3di, std::list<vector3di>*>::iterator mit = chunk->multiBlockLists.begin();
	std::list<std::list<vector3di>*> todelete;
	while (mit != chunk->multiBlockLists.end())
	{
		if (std::find(todelete.begin(), todelete.end(), mit->second) == todelete.end())
		{
			todelete.push_back(mit->second);
			mit->second->clear();
			delete mit->second;
		}
		++mit;
	}
	chunk->multiBlockLists.clear();

	// delete any block details that have inventories
	map<vector3di, BlockDetails>::iterator dit = chunk->blockDetails.begin();
	while (dit != chunk->blockDetails.end())
	{
		if (dit->second.inventory)
		{
			// clear this inventory
			for (GLuint i = 0; i < dit->second.inventory->size(); i++) {
				delete dit->second.inventory->at(i);
			}
			delete dit->second.inventory;
		}

		++dit;
	}
	chunk->blockDetails.clear();

	// delete the chunk
	delete chunk;
}

bool TerrainManager::LoadBiomes()
{
	// read in the biomes XML file and create the biomes voronoi diagram to use for biome generation
	// create an XML file reader
	XMLReader XML(m_BiomesFile);
	if (!XML.IsOpen())
		return false;

	// read the file
	const string strBiomesTag("biomes");
	const string type("biome");
	string strCurrentSection, strNodeName;
	Biome biome;
	bool bLoaded = false;

	// push back the sea biome (this one always exists in the game)
	layer _layer;
	_layer.block = 1;
	_layer.depth = 2;
	_layer.smooth = false;
	biome.name = "Ocean";
	biome.layers.emplace(-5, _layer);
	biome.index = 0;
	m_Biomes.push_back(biome);

	while (XML.ReadLine())
	{
		if (XML.IsNode(strBiomesTag))
			strCurrentSection = strBiomesTag;
		else if (strCurrentSection == strBiomesTag && XML.IsNode(type))
			strCurrentSection = type;
		else if (strCurrentSection == type)
		{
			// check if we have reached the end of the item
			if (XML.IsEndOfNode(strCurrentSection))
			{
				if (strCurrentSection == type)
				{
					biome.index = m_Biomes.size();
					m_Biomes.push_back(biome);
				}
				
				strCurrentSection = strBiomesTag;

				continue;
			}

			// look for what we need
			strNodeName = XML.GetNodeName();
			if (strNodeName == "name")
			{
				biome.funcs.clear();
				biome.layers.clear();
				biome.misc.clear();
				biome.ores.clear();
				biome.name = XML.GetAttributeValue("value");

				bLoaded = true; // we have loaded at least one so return true
			}
			else if (strNodeName == "height_func")
			{
				height_func fn;

				string tmp = XML.GetAttributeValue("mode");
				if (tmp == "add")
					fn.mode = 1;
				else
					fn.mode = -1;

				fn.smoothx = atof(XML.GetAttributeValue("smoothx").c_str());
				fn.smoothy = atof(XML.GetAttributeValue("smoothy").c_str());
				fn.amplify = atof(XML.GetAttributeValue("amplify").c_str());

				biome.funcs.push_back(fn);
			}
			else if (strNodeName == "layer")
			{
				layer l;

				l.block = Item::GetItemClass(XML.GetAttributeValue("block"))->index;
				l.depth = -atoi(XML.GetAttributeValue("depth").c_str());
				l.smooth = (XML.GetAttributeValue("smooth") == "true" ? true : false);

				biome.layers.emplace(l.depth, l);
			}
			else if (strNodeName == "ore")
			{
				ore_spawn ore;

				ore.index = Item::GetItemClass(XML.GetAttributeValue("name"))->index;
				ore.density = atoi(XML.GetAttributeValue("density").c_str());
				ore.max = atoi(XML.GetAttributeValue("max").c_str());
				ore.upper = atoi(XML.GetAttributeValue("upper").c_str());
				ore.lower = atoi(XML.GetAttributeValue("lower").c_str());

				biome.ores.push_back(ore);
			}
			else if (strNodeName == "extra")
			{
				object_gen gen;

				gen.index = Item::GetItemClass(XML.GetAttributeValue("name"))->index;
				gen.depth = -atoi(XML.GetAttributeValue("depth").c_str());
				gen.chance = atoi(XML.GetAttributeValue("chance").c_str());

				biome.misc.push_back(gen);
			}
		}
	}

	// we have completed successfully
	return bLoaded;
}

void TerrainManager::WriteDataFile(string filename, const vector3di& chunkPos, unsigned char* data, int size)
{
	// write the data to the given data file, updating the header and other file contents if needs be
	// figure out what group we are in
	vector3di groupPos = GetGroupPos(chunkPos);
	vector3di innerPos = vector3di(chunkPos) - (groupPos * 8);

	// determine if the chunk group file for this chunk exists
	if (!FileExists(filename))
	{
		// this is the first chunk to be written to this chunk group, write a new blank file with the header
		std::ofstream file(filename, std::ios::binary);

		// the header is 2KB, consiting of 512 unsigned integers representing each chunk's data position in the file
		char* buf = new char[2048];
		memset(buf, 0, 2048);
		file.write(buf, 2048);
		delete[] buf;

		// that's it for now, close the file
		file.close();
	}

	// the file should exist by this point, so open it for writing and reading (so we don't overwite existing data)
	std::fstream file(filename, std::ios::in | std::ios::out | std::ios::binary);
	file.seekg(0, std::ios::beg);

	// make sure it is open
	if (!file.is_open())
		return;

	// read in the header
	unsigned char* header = new unsigned char[2048];
	file.read((char*)header, 2048);

	// function to read chunk data positions to an integer
	auto header_to_chunk_position = [&header](int pos) -> int {
		int ret;
		ByteToInt(&header[pos], ret);

		return ret;
	};

	// figure out where we are in this file
	int chunkSizePos = (innerPos.x * 8 * 8 + innerPos.z * 8 + innerPos.y) * 4;
	int startpos = header_to_chunk_position(chunkSizePos);
	std::streampos epos = FileSize(file);

	// if the start position is 0 then we will write to the end of the file
	if (startpos < 2048)
		startpos = static_cast<int>(epos);

	// now we need to figure out the end position of this chunk's data
	int endpos = static_cast<int>(epos);
	for (GLuint i = 0; i < CHUNK_GROUP_SIZE; i++) {
		int pos = header_to_chunk_position(i * 4);

		if (pos > startpos && pos < endpos)
			endpos = pos;
	}

	// write the size to our chunk's position in the header
	file.seekg(std::streampos(chunkSizePos), std::ios::beg);
	unsigned char tmp[4];
	IntToByte(startpos, tmp);

	file.write((char*)tmp, 4);
	for (GLuint i = 0; i < 4; i++) {
		header[chunkSizePos + i] = tmp[i];
	}

	// if this isn't the first time we've written to the file, check to see if we're taking up more space than before
	// if we're using less then we don't really care, no point in shrinking the file (because it could just expand again later)
	if (endpos != startpos)
	{
		// if endpos != startpos then we aren't writing the chunk for the first time
		int diff = startpos + size - endpos;
		if (diff != 0)
		{
			// we need to move everything after this chunk's data along by diff, including changing their positions
			// stop anything funny from happening
			int len = static_cast<int>(epos) - endpos;

			// let us first change the header
			if (len > 0)
			{
				for (GLuint i = 0; i < CHUNK_GROUP_SIZE; i++) {
					if (i * 4 == chunkSizePos)
						continue;

					int _pos = header_to_chunk_position(i * 4);
					if (_pos > startpos)
					{
						// this chunk's data comes after ours, update the header details for this chunk
						_pos += diff;
						IntToByte(_pos, &header[i * 4]);
					}
				}

				// now we have updated the header as necessary, write it to the file
				file.seekg(0, std::ios::beg);
				file.write((char*)header, 2048);

				// now we can move the rest of the data along
				char* tmp = new char[len];
				file.seekg(std::streampos(endpos), std::ios::beg);
				file.read(tmp, len);
				file.seekg(std::streampos(endpos + diff), std::ios::beg);
				file.write(tmp, len);
				delete[] tmp;
			}
		}

		// write over the current data with 0
		/*file.seekg(std::streampos(startpos), std::ios::beg);
		for (int i = 0; i < max(size, endpos - startpos); i++) {
			file << '\0';
		}*/
		//file.write(0, max(size, endpos - startpos));
	}

	// now go to the position and write the data to the file
	file.seekg(std::streampos(startpos), std::ios::beg);
	file.write((char*)data, size);

	// we have written the chunk to the disk! let us clean up
	file.close();
	delete[] header;
}

bool TerrainManager::ReadDataFile(string filename, const vector3di& chunkPos, unsigned char* data, int& size)
{
	// read the data for the given file - ensure data is large enough to store the data
	// set the size to 0
	size = 0;

	// figure out what group we are in
	vector3di groupPos = GetGroupPos(chunkPos);
	vector3di innerPos = vector3di(chunkPos) - (groupPos * 8);

	// if the file doesn't exist then we obviously can't load it
	if (!FileExists(filename))
		return false;

	// the file exists, open it and see if we have any data stored in the file
	std::fstream file(filename, std::ios::in | std::ios::binary);
	int chunkSizePos = (innerPos.x * 8 * 8 + innerPos.z * 8 + innerPos.y) * 4;

	// read in the header
	unsigned char* header = new unsigned char[2048];
	file.read((char*)header, 2048);

	// function to read chunk data positions to an integer
	auto header_to_chunk_position = [&header](int pos) -> int {
		int ret;
		ByteToInt(&header[pos], ret);

		return ret;
	};

	// read in the position of this chunk's data
	int startpos = header_to_chunk_position(chunkSizePos);

	// if the start position is 0 then there is no data saved for this chunk
	if (startpos == 0)
	{
		file.close();
		delete[] header;
		return false;
	}

	// now we need to figure out the end position of this chunk's data
	int endpos = static_cast<int>(FileSize(file));
	for (GLuint i = 0; i < CHUNK_GROUP_SIZE; i++) {
		int pos = header_to_chunk_position(i * 4);

		if (pos > startpos && pos < endpos)
			endpos = pos;
	}

	// read in the data
	size = endpos - startpos;
	file.seekg(std::streampos(startpos), std::ios::beg);
	file.read((char*)data, size);

	// clean up
	file.close();
	delete[] header;
	return true;
}

string TerrainManager::GroupFileName(const vector3di& groupPos, string path)
{
	// get a string of the filename for this group position
	std::stringstream filename;
	filename << "save/" << m_WorldName << "/" << path << "/g_" << groupPos.x << "_" << groupPos.y << "_" << groupPos.z;

	// return the string
	return ToLower(filename.str());
}

vector3di TerrainManager::GetGroupPos(const vector3di& chunkPos)
{
	// figure out what group we are in
	vector3di cpos(chunkPos);
	if (chunkPos.x < 0) cpos.x++;
	if (chunkPos.y < 0) cpos.y++;
	if (chunkPos.z < 0) cpos.z++;
	vector3di groupPos = vector3di(cpos) / 8;
	if (chunkPos.x < 0) groupPos.x--;
	if (chunkPos.y < 0) groupPos.y--;
	if (chunkPos.z < 0) groupPos.z--;

	// return the group position
	return groupPos;
}

void TerrainManager::SaveChunk(const vector3di& chunkPos)
{
	// save the chunk in the given position to the chunk group file
	// check the chunk actually exists
	if (m_Terrain.find(chunkPos) == m_Terrain.end())
		return;

	// the chunk exists, save its data to the disk
	vector3di groupPos = GetGroupPos(chunkPos);
	TerrainChunk* chunk = m_Terrain.at(chunkPos);

	// compress the terrain data
	Bytef* buf = new Bytef[CHUNK_BUFFER_SIZE];
	Bytef* data = reinterpret_cast<Bytef*>(chunk->terrain);
	uLongf size = CHUNK_BUFFER_SIZE;
	compress(buf, &size, data, CHUNK_BUFFER_SIZE);

	// get the name of the chunk group file
	string filename = GroupFileName(groupPos).append(".dat");

	// write the file
	WriteDataFile(filename, chunkPos, buf, size);

	// we have written the chunk to the disk! let us clean up
	delete[] buf;
}

void TerrainManager::SaveWater(const vector3di& chunkPos)
{
	// save the chunk in the given position to the chunk group file
	// check the chunk actually exists
	if (m_Terrain.find(chunkPos) == m_Terrain.end())
		return;

	// the chunk exists, save its data to the disk
	vector3di groupPos = GetGroupPos(chunkPos);
	TerrainChunk* chunk = m_Terrain.at(chunkPos);

	// compress the terrain data
	Bytef* buf = new Bytef[CHUNK_WATER_BUFFER_SIZE];
	Bytef* data = reinterpret_cast<Bytef*>(chunk->currentWater);
	uLongf size = CHUNK_WATER_BUFFER_SIZE;
	compress(buf, &size, data, CHUNK_WATER_BUFFER_SIZE);

	// get the name of the chunk group file
	string filename = GroupFileName(groupPos).append(".wtd");

	// write the file
	WriteDataFile(filename, chunkPos, buf, size);

	// we have written the chunk to the disk! let us clean up
	delete[] buf;
}

void TerrainManager::SaveMultiblockLists(const vector3di& chunkPos)
{
	// save this chunk's multi block lists to a file
	// check the chunk actually exists
	if (m_Terrain.find(chunkPos) == m_Terrain.end())
		return;

	// the chunk exists, save its data to the disk
	vector3di groupPos = GetGroupPos(chunkPos);
	TerrainChunk* chunk = m_Terrain.at(chunkPos);

	// multi-block lists will be compacted as such: length of list, list data, length of list...
	map<vector3di, std::list<vector3di>*>::iterator it = chunk->multiBlockLists.begin();
	vector<char> data;
	std::list<std::list<vector3di>*> added;
	while (it != chunk->multiBlockLists.end())
	{
		if (std::find(added.begin(), added.end(), it->second) == added.end())
		{
			// we have not yet added the information for this list, do so now
			std::list<vector3di>* list = it->second;

			data.push_back(static_cast<char>(list->size()));
			std::list<vector3di>::iterator lit = list->begin();
			while (lit != list->end())
			{
				// push back the x, y and z details for this block
				data.push_back(static_cast<char>((*lit).x));
				data.push_back(static_cast<char>((*lit).y));
				data.push_back(static_cast<char>((*lit).z));

				lit++;
			}

			// add this list to the added list so that we do not write it more than once
			added.push_back(list);
		}

		it++;
	}

	// if there is no data to write, exit
	if (data.empty())
		return;

	// get the name of the chunk group file
	string filename = GroupFileName(groupPos, "chunkdetail").append(".mbl");

	// compress the terrain data
	uLongf size = CHUNK_BUFFER_SIZE;
	Bytef* buf = new Bytef[size];
	memset(buf, 0, size);
	compress(buf, &size, (Bytef*)&data[0], data.size());

	// write the data to the disk
	WriteDataFile(filename, chunkPos, buf, size);

	// clean up
	delete[] buf;
}

void TerrainManager::SaveBlockDetails(const vector3di& chunkPos)
{
	// save this chunk's block details to a file
	// check the chunk actually exists
	if (m_Terrain.find(chunkPos) == m_Terrain.end())
		return;

	// the chunk exists, save its data to the disk
	vector3di groupPos = GetGroupPos(chunkPos);
	TerrainChunk* chunk = m_Terrain.at(chunkPos);

	// multi-block lists will be compacted as such: length of list, list data, length of list...
	map<vector3di, BlockDetails>::iterator it = chunk->blockDetails.begin();
	vector<unsigned char> data;
	while (it != chunk->blockDetails.end())
	{
		// write the inventory to the data vector
		vector<Item*>* inv = it->second.inventory;
		if (inv)
		{
			// push back the vector position for these details
			data.push_back(it->first.x);
			data.push_back(it->first.y);
			data.push_back(it->first.z);

			// push back the size of the inventory
			data.push_back(inv->size());

			// push back the number of items we are writing
			data.push_back(GetItemCount(inv));

			// push back the items in the inventory
			InventoryToData(inv, data);
		}

		it++;
	}

	// if there is no data to write, exit
	if (data.empty())
		return;

	// get the name of the chunk group file
	string filename = GroupFileName(groupPos, "chunkdetail").append(".bld");

	// compress the terrain data
	uLongf size = CHUNK_BUFFER_SIZE;
	Bytef* buf = new Bytef[size];
	memset(buf, 0, size);
	compress(buf, &size, &data[0], data.size());

	// write the data to the disk
	WriteDataFile(filename, chunkPos, buf, size);

	// clean up
	delete[] buf;
}

void TerrainManager::SaveTerrain()
{
	// loop through all our terrain chunks and write them to the disk
	TerrainMap::iterator it = m_Terrain.begin();
	m_bSaving = true;

	while (it != m_Terrain.end())
	{
		SaveChunk(it->first);
		SaveWater(it->first);
		SaveMultiblockLists(it->first);
		SaveBlockDetails(it->first);

		++it;
	}

	m_bSaving = false;
}

void TerrainManager::Shutdown()
{
	m_bShuttingDown = true;
}

void TerrainManager::Update()
{
	// keep a loop running whilst the server is going
	// we will deal with terrain generation and sending terrain packets
	// generate the spawn chunks
	_out << "Generating spawn chunks..." << std::endl;
	for (int x = -4; x <= 4; x++) {
		for (int z = -4; z <= 4; z++) {
			for (int y = -3; y <= 3; y++) {
				vector3di pos(x, y, z);
				if (m_Terrain.find(pos) == m_Terrain.end())
				{
					TerrainChunk* c = new TerrainChunk;
					m_Terrain.emplace(pos, c);
				}

				if (!m_Terrain.at(pos)->bGenerated)
					GenerateTerrain(pos);
			}
		}
		_out << (int)(((((float)x + 5) * 9 * 7) / 567.f) * 100) << "% complete" << std::endl;
	}

	// let the user know spawn chunks are created
	_out << "Spawn chunks have been created..." << _end;

	// let the user know we are accepting commands
	_out << "Server now running! Listening for terminal input..." << std::endl << _end;

	// function used in lighting updates
	auto compare_block_rgb = [&](bool& highest, vector3di blockPos, vector3di chunkPos, GLuint& r, GLuint& g, GLuint& b) {
		if (!highest)
		{
			PositionCorrect(blockPos, chunkPos);
			TerrainBlock tb = _GetBlock(blockPos, chunkPos);
			if (tb.LightRed > r || tb.LightGreen > g || tb.LightBlue > b)
			{
				r = tb.LightRed;
				g = tb.LightGreen;
				b = tb.LightBlue;
				if (r == 15 || g == 15 || b == 15)
					highest = true;
				return true;
			}
		}

		return false;
	};

	// function used in lighting updates
	auto compare_block_sky = [&](bool& highest, vector3di blockPos, vector3di chunkPos, GLuint& v) {
		if (!highest)
		{
			PositionCorrect(blockPos, chunkPos);
			TerrainBlock tb = _GetBlock(blockPos, chunkPos);
			if (tb.SkyLight > v)
			{
				v = tb.SkyLight;
				if (v == 15)
					highest = true;
				return true;
			}
		}

		return false;
	};

	// function to update block lighting
	auto update_lighting = [&](vector3di blockPos, vector3di chunkPos) {
		if (!m_BlockLightCreation.empty())
		{
			UpdateLightCreation();
			//return;
		}

		vector3di cpos, bpos;
		TerrainBlock tb;
		bool bHighest = false;
		GLuint r = 0;
		GLuint g = 0;
		GLuint b = 0;

		if (compare_block_rgb(bHighest, blockPos + vector3di(1, 0, 0), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(1, 0, 0);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(-1, 0, 0), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(-1, 0, 0);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(0, 1, 0), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(0, 1, 0);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(0, -1, 0), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(0, -1, 0);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(0, 0, 1), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(0, 0, 1);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(0, 0, -1), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(0, 0, -1);
			cpos = chunkPos;
		}

		PositionCorrect(bpos, cpos);
		SetBlockLight(bpos, cpos, r, g, b);
	};

	// function to update skylighting
	auto update_skylighting = [&](vector3di blockPos, vector3di chunkPos) {
		if (!m_SunLightCreation.empty())
		{
			UpdateSunlightCreation();
			//return;
		}

		vector3di cpos, bpos;
		TerrainBlock tb;
		bool bHighest = false;
		GLuint v = 0;

		if (compare_block_sky(bHighest, blockPos + vector3di(1, 0, 0), chunkPos, v))
		{
			bpos = blockPos + vector3di(1, 0, 0);
			cpos = chunkPos;
		}
		if (compare_block_sky(bHighest, blockPos + vector3di(-1, 0, 0), chunkPos, v))
		{
			bpos = blockPos + vector3di(-1, 0, 0);
			cpos = chunkPos;
		}
		if (compare_block_sky(bHighest, blockPos + vector3di(0, -1, 0), chunkPos, v))
		{
			bpos = blockPos + vector3di(0, -1, 0);
			cpos = chunkPos;
		}
		if (compare_block_sky(bHighest, blockPos + vector3di(0, 0, 1), chunkPos, v))
		{
			bpos = blockPos + vector3di(0, 0, 1);
			cpos = chunkPos;
		}
		if (compare_block_sky(bHighest, blockPos + vector3di(0, 0, -1), chunkPos, v))
		{
			bpos = blockPos + vector3di(0, 0, -1);
			cpos = chunkPos;
		}

		{
			vector3di c(chunkPos), b(blockPos + vector3di(0, 1, 0));
			PositionCorrect(b, c);
			tb = _GetBlock(b, c);
			if (tb.SkyLight > 0)
				SetSunLight(b, c, tb.SkyLight);
		}

		PositionCorrect(bpos, cpos);
		SetSunLight(bpos, cpos, v);
	};

	// update the terrain
	while (!m_bShuttingDown)
	{
		// if we have any lights to update, do so now
		//m_Mutex.lock();
		if (!m_LightUpdates.empty())
		{
			// copy the light updates vector and erase it
			vector<LightUpdateNode> nodes(m_LightUpdates);
			m_LightUpdates.clear();

			// now loop through each node and update the terrain's lighting
			for (GLuint i = 0; i < nodes.size(); i++) {
				TerrainBlock b = GetBlock(nodes[i].chunkPos, nodes[i].blockPos);
				const ItemClass* ic = Item::GetItemClass(b);

				// determine whether we are removing light
				bool _l = !b.Ignored;
				if (ic && ic->LightColour != vector3di(0))
					_l = false;

				// remove block light at this position
				if (_l || (nodes[i].old.LightRed > 0 || nodes[i].old.LightBlue > 0 || nodes[i].old.LightGreen > 0))
					RemoveBlockLight(nodes[i].blockPos, nodes[i].chunkPos, nodes[i].old);

				// remove the sky light at this position
				if (!b.Ignored)
					RemoveSunLight(nodes[i].blockPos, nodes[i].chunkPos, nodes[i].old);

				// update the lighting at this block
				if (!ic || ic->LightColour == vector3di(0))
					update_lighting(nodes[i].blockPos, nodes[i].chunkPos);
				else if (ic)
					SetBlockLight(nodes[i].blockPos, nodes[i].chunkPos, ic->LightColour.x, ic->LightColour.y, ic->LightColour.z);
				update_skylighting(nodes[i].blockPos, nodes[i].chunkPos);
			}
		}

		// get a list of the clients on the server
		std::vector<Client*> clients;
		m_pServer->GetListOfClients(clients);

		// update water
		bool bSendWaterUpdate = true;
		bool bCanDelete(true);
		TerrainMap::iterator it = m_Terrain.begin();
		while (!m_bShuttingDown && !m_bSaving && it != m_Terrain.end() && it->second)
		{
			// if we haven't yet generated this chunk, do so now
			if (!it->second->bGenerated && it->second->bNeedsGeneration)
			{
				GenerateTerrain(it->first);
				//_out << it->first.x << ", " << it->first.y << ", " << it->first.z << _end;
			}

			// flag to say if we've deleted this chunk or not
			bool bDeleted(false);
			
			TerrainChunk* chunk = it->second;
			if (chunk && !chunk->bWaterSettled)
			{
				// set the flag to true - it will get set to false again later in the code if we need to update again
				chunk->bWaterSettled = true;

				// variables needed to update
				GLfloat flow = 0.f;
				GLfloat waterLeft = 0.f;
				vector3di blockPos, chunkPos(it->first);
				TerrainBlock* block = NULL;
				TerrainChunk* other = NULL;
				GLint x, y, z;

				// function to update water
				auto update_water = [&](const vector3di& offset, bool force = false) -> bool {
					blockPos.Set(x + offset.x, y + offset.y, z + offset.z);
					chunkPos.Set(it->first);

					block = _BlockAt(blockPos, chunkPos, &other);
					if (block && block->BlockType == 0 && GetNewWater(other, blockPos) < 1.f)
					{
						// there is a free space in this block, let us flow into it
						vector3di here(x, y, z);
						if (force)
						{
							// we are forcing (ie flowing down), try and dump more water beneath us
							flow = (1.f - GetNewWater(other, blockPos)) * 0.5f;
							Limit(flow, 0.f, GetNewWater(chunk, here));
						}
						else
						{
							// we aren't forcing (ie we are spreading to the sides), use a normal calculation
							flow = (GetWater(chunk, here) - GetNewWater(other, blockPos)) * 0.5f;
							if (flow > 0.1f)
								flow *= 0.5f;
						}

						bool bEven = round(abs(flow * 250.f)) <= 1.f;

						Limit(flow, 0.f, waterLeft);
						if (bEven && !force)
						{
							SetNewWater(other, blockPos, GetNewWater(chunk, here));
						}
						else
						{
							SetNewWater(chunk, here, GetNewWater(chunk, here) - flow);
							SetNewWater(other, blockPos, GetNewWater(other, blockPos) + flow);
						}
						waterLeft -= flow;

						// set the settled flags to false
						chunk->bWaterSettled = false;
						other->bWaterSettled = false;
						bSendWaterUpdate = false;

						// we have updated water
						return flow > 0.02f;
					}

					// we haven't updated any water
					return false;
				};

				// go through and update the water!
				for (x = 0; x < CHUNK_SIZE; x++) {
					for (z = 0; z < CHUNK_SIZE; z++) {
						for (y = 0; y < CHUNK_SIZE; y++) {
							// if there isn't water here, continue
							if (chunk->terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y].Water == 0)
								continue;

							// calculate water spread
							flow = 0.f;
							waterLeft = GetWater(chunk, vector3di(x, y, z));
							if (waterLeft <= 0.f)
								continue;

							// below
							if (!update_water(vector3di(0, -1, 0), true) && waterLeft > 0.f)
							{
								// up
								update_water(vector3di(0, 0, 1));
								if (waterLeft <= 0.f)
									continue;

								// down
								update_water(vector3di(0, 0, -1));
								if (waterLeft <= 0.f)
									continue;

								// left
								update_water(vector3di(1, 0, 0));
								if (waterLeft <= 0.f)
									continue;

								// right
								update_water(vector3di(-1, 0, 0));
							}
						}
					}
				}

				// update the current water status
				//std::list<vector3di>::iterator it;
				for (x = 0; x < CHUNK_SIZE; x++) {
					for (z = 0; z < CHUNK_SIZE; z++) {
						for (y = 0; y < CHUNK_SIZE; y++) {
							// if there isn't water here, continue
							if (chunk->terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y].BlockType > 0)
								continue;

							// sort out water list
							vector3di here(x, y, z);
							GLfloat current = GetWater(chunk, here);
							GLfloat next = GetNewWater(chunk, here);
							if (next > 0.02f)
							{
								// there is now water in this block, add it to the list
								chunk->terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y].Water = 1;
							}

							SetWater(chunk, here, next);

							if (next <= 0.02f)
							{
								// there used to be water here but now there isn't, remove us from the list
								chunk->terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y].Water = 0;
							}
						}
					}
				}
			}
			else if (chunk && bCanDelete && chunk->bGenerated)
			{
				// see if this chunk is out of range of any client / spawn and if so, delete it and write it to disk
				bool bDelete = !(abs(it->first.x) <= 5 && abs(it->first.y) <= 4 && abs(it->first.z) <= 5);

				if (!clients.empty())
				{
					for (GLuint i = 0; i < clients.size() && bDelete; ++i) {
						vector3di blockPos, chunkPos;
						ConvertToChunkBlockPosition(clients[i]->data.pos, blockPos, chunkPos);

						if (abs(chunkPos.x - it->first.x) <= 4 && abs(chunkPos.y - it->first.y) <= 3 && abs(chunkPos.z - it->first.z) <= 4)
						{
							// this chunk is in range, we will do nothing to this chunk
							bDelete = false;
						}
					}
				}

				if (bDelete)
				{
					// save this chunk
					m_bSaving = true;
					SaveChunk(it->first);
					SaveWater(it->first);
					SaveMultiblockLists(it->first);
					SaveBlockDetails(it->first);

					// delete this chunk
					DeleteChunk(it->second);
					bDeleted = true;
					bCanDelete = false;
					m_bSaving = false;
				}
			}

			if (bDeleted)
				it = m_Terrain.erase(it);
			else
				it++;
		}

		// loop through each connected client and ensure the chunks around them are loaded in memory
		if (!clients.empty())
		{
			TerrainMap::iterator it;
			for (GLuint i = 0; i < clients.size(); ++i) {
				vector3di blockPos, chunkPos;
				ConvertToChunkBlockPosition(clients[i]->data.pos, blockPos, chunkPos);

				for (int x = chunkPos.x - 4; x < chunkPos.x + 5; ++x) {
					for (int z = chunkPos.z - 4; z < chunkPos.z + 5; ++z) {
						for (int y = chunkPos.y - 3; y < chunkPos.y + 3; ++y) {
							vector3di pos(x, y, z);
							it = m_Terrain.find(pos);

							if (it == m_Terrain.end() || !it->second)
							{
								// the chunk here is out of memory, re-create it
								TerrainChunk* chunk = new TerrainChunk;
								chunk->bNeedsGeneration = true;
								m_Terrain.emplace(pos, chunk);
							}
						}
					}
				}
			}
		}

		// sleep for 1/4 of a second
		//m_Mutex.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(20));
	}
}

TerrainBlock* TerrainManager::_BlockAt(vector3di& blockPos, vector3di& chunkPos, TerrainChunk** out)
{
	// correct the position
	PositionCorrect(blockPos, chunkPos);

	// get the block at this position
	*out = GetChunk(chunkPos);
	if (*out)
		return &(*out)->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y];

	return NULL;
}

void TerrainManager::SetWater(TerrainChunk* chunk, const vector3di& pos, GLfloat water)
{
	Limit(water, 0.f, 1.f);
	chunk->currentWater[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] = water * 250.f;
}

void TerrainManager::SetNewWater(TerrainChunk* chunk, const vector3di& pos, GLfloat water)
{
	Limit(water, 0.f, 1.f);
	chunk->newWater[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] = water * 250.f;
}

GLfloat TerrainManager::GetWater(TerrainChunk* chunk, const vector3di& pos) const
{
	return chunk->currentWater[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] * 0.004f;
}

GLfloat TerrainManager::GetNewWater(TerrainChunk* chunk, const vector3di& pos) const
{
	return chunk->newWater[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] * 0.004f;
}

TerrainBlock TerrainManager::GetBlock(vector3di cpos, vector3di pos)
{
	// if pos is out of bounds we're looking at the next chunk along
	PositionCorrect(pos, cpos);
	
	// get the chunk
	TerrainChunk* c = m_Terrain.at(cpos);
	if (c)
	{
		return c->terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y];
	}

	static TerrainBlock space(0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0);
	return space;
}

TerrainBlock TerrainManager::GetBlock(vector3df pos)
{
	// make sure that a chunk exists in this location
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// the chunk exists
		int x = blockPos.x;
		int y = blockPos.y;
		int z = blockPos.z;

		return pChunk->terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y];
	}

	return TerrainBlock(0);
}

bool TerrainManager::GetPositionInChunk(vector3df& pos, TerrainChunk*& chunk, vector3di& out, vector3di& chunkPos) const
{
	// figure out which chunk we are in
	chunkPos.Set((GLint)(pos.x * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.y * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.z * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE));
	if (pos.x < 0) chunkPos.x -= 1;
	if (pos.y < 0) chunkPos.y -= 1;
	if (pos.z < 0) chunkPos.z -= 1;

	// make sure that a chunk exists in this location
	if (m_Terrain.find(chunkPos) != m_Terrain.end())
	{
		chunk = m_Terrain.at(chunkPos);

		if (chunk)
		{
			// the chunk exists, let's get the next block below the current position
			int x = (int)(pos.x * ONE_OVER_BLOCK_SIZE) - chunkPos.x * CHUNK_SIZE;
			int y = (int)(pos.y * ONE_OVER_BLOCK_SIZE) - chunkPos.y * CHUNK_SIZE;
			int z = (int)(pos.z * ONE_OVER_BLOCK_SIZE) - chunkPos.z * CHUNK_SIZE;

			// deal with negative chunks
			if (pos.x < 0) x -= 1;
			if (pos.y < 0) y -= 1;
			if (pos.z < 0) z -= 1;

			// output
			out.Set(x, y, z);
			return true;
		}
	}

	return false;
}

void TerrainManager::ConvertToChunkBlockPosition(const vector3df& pos, vector3di& blockPos, vector3di& chunkPos) const
{
	// chunk position
	chunkPos.Set((GLint)(pos.x * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.y * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.z * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE));
	if (pos.x < 0) chunkPos.x -= 1;
	if (pos.y < 0) chunkPos.y -= 1;
	if (pos.z < 0) chunkPos.z -= 1;

	// block position
	int x = (int)(pos.x * ONE_OVER_BLOCK_SIZE) - chunkPos.x * CHUNK_SIZE;
	int y = (int)(pos.y * ONE_OVER_BLOCK_SIZE) - chunkPos.y * CHUNK_SIZE;
	int z = (int)(pos.z * ONE_OVER_BLOCK_SIZE) - chunkPos.z * CHUNK_SIZE;

	// deal with negative chunks
	if (pos.x < 0) x -= 1;
	if (pos.y < 0) y -= 1;
	if (pos.z < 0) z -= 1;

	// output
	blockPos.Set(x, y, z);
}

void TerrainManager::CleanUpVacantChunks()
{
	// this should get called every couple of minutes (or every so often) to clear up any unused chunks
	std::vector<Client*> clients;
	m_pServer->GetListOfClients(clients);

	// if there are no clients connected, delete ALL CHUNKS from memory! (after writing them to disk)
	if (clients.empty())
	{
		// save the terrain
		m_Mutex.lock();
		SaveTerrain();

		// delete all chunks
		TerrainMap::iterator it = m_Terrain.begin();

		while (it != m_Terrain.end())
		{
			DeleteChunk(it->second);

			++it;
		}

		m_Terrain.clear();
		m_Mutex.unlock();
	}
	else
	{
		// go through each chunk and delete/write to disk the ones which aren't in range of any clients
		TerrainMap::iterator it = m_Terrain.begin();
		m_bSaving = true;

		while (it != m_Terrain.end())
		{
			// figure out if we are near any clients
			bool bNearClients(false);
			for (GLuint i = 0; i < clients.size() && !bNearClients; ++i) {
				vector3di blockPos, chunkPos;
				ConvertToChunkBlockPosition(clients[i]->data.pos, blockPos, chunkPos);

				if ((abs(chunkPos.x - it->first.x) <= 7 && abs(chunkPos.y - it->first.y) <= 3 && abs(chunkPos.z - it->first.z) <= 7) ||
					(abs(it->first.x) <= 7 && abs(it->first.y) <= 4 && abs(it->first.z) <= 7))
				{
					// this chunk is in range, we will do nothing to this chunk
					bNearClients = true;
				}
			}
			
			if (!bNearClients)
			{
				// save this chunk
				m_bSaving = true;
				m_Mutex.lock();
				SaveChunk(it->first);
				SaveWater(it->first);
				SaveMultiblockLists(it->first);
				SaveBlockDetails(it->first);

				// delete this chunk
				DeleteChunk(it->second);
				it = m_Terrain.erase(it);
				m_Mutex.unlock();
				m_bSaving = false;
			}
			else
				++it;
		}
	}
}

bool PointInTriangle(vector3df& p, vector3df& p1, vector3df& p2, vector3df& p3)
{
	// note this checks against a 2d triangle and is mainly used for the GetHeightBelow when dealing with half blocks
	GLfloat a = 0.5f * (-p2.z * p3.x + p1.z * (-p2.x + p3.x) + p1.x * (p2.z - p3.z) + p2.x * p3.z);
	GLfloat sign = a < 0 ? -1.f : 1.f;
	GLfloat s = (p1.z * p3.x - p1.x * p3.z + (p3.z - p1.z) * p.x + (p1.x - p3.x) * p.z) * sign;
	GLfloat t = (p1.x * p2.z - p1.z * p2.x + (p1.z - p2.z) * p.x + (p2.x - p1.x) * p.z) * sign;

	return (s > 0 && t > 0 && s + t < 2 * a * sign);
}

GLfloat GetHeightOnTriangle(vector3df& p, vector3df& p1, vector3df& p2, vector3df& p3)
{
	// gets the height on this triangle - used for when colliding with half-block slopes as the GetHeightOnFace doesn't work well for those situations
	/*float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);

	float h1 = ((p2.z - p3.z) * (p.x - p3.x) + (p3.x - p2.x) * (p.z - p3.z)) / det;
	float h2 = ((p3.z - p1.z) * (p.x - p3.x) + (p1.x - p3.x) * (p.z - p3.z)) / det;
	float h3 = 1.f - h1 - h2;

	return (h1 * p1.y + h2 * p2.y + h3 * p3.y);*/
	triangle3df t(p1, p2, p3);
	vector3df n = -t.Normal().Normal();

	GLfloat d = -((n.x * p1.x) + (n.y * p1.y) + (n.z * p1.z));

	return -((n.x * p.x) + (n.z * p.z) + d) / (n.y);
}

GLfloat GetHeightOnFace(vector3df& p, vector3df& p1, vector3df& p2, vector3df& p3, vector3df& p4)
{
	//           p4 - - - p3 
	//           |         |
	//           |         |
	//           |         |
	//           p2 - - - p1

	// get the y-coordinate on a face at point (p.x, p.z)
	vector2df offset((p.x - p1.x) * ONE_OVER_BLOCK_SIZE, (p.z - p1.z) * ONE_OVER_BLOCK_SIZE);
	GLfloat a = (p4.y * offset.x + p3.y * (1 - offset.x)) * offset.y;
	GLfloat b = (p2.y * offset.x + p1.y * (1 - offset.x)) * (1 - offset.y);

	return a + b;
}

GLfloat TerrainManager::GetHeightBelow(vector3df pos) const
{
	// make sure that a chunk exists in this location
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// the chunk exists
		int x = blockPos.x;
		int y = blockPos.y;
		int z = blockPos.z;

		//bool searching = true;
		int height = (int)(pos.y * ONE_OVER_BLOCK_SIZE);
		if (pos.y < 0) height--;
		while (true)
		{
			for (; y >= 0; y--, height--) {
				TerrainBlock block = pChunk->terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y];
				if (block.Solid)
				{
					if (!block.XX && !block.XY && !block.YX && !block.YY)
						return (height + 1) * BLOCK_SIZE;

					// if this is a half block (ie half slope corner) then check if we're in the visible triangle and if so, get the height on the triangle otherwise fall through to the next block below
					vector3df p(chunkPos.x * CHUNK_SIZE * BLOCK_SIZE + x * BLOCK_SIZE, chunkPos.y * CHUNK_SIZE * BLOCK_SIZE + (y + 1) * BLOCK_SIZE, chunkPos.z * CHUNK_SIZE * BLOCK_SIZE + z * BLOCK_SIZE);
					if (block.Half)
					{
						vector3df p1, p2, p3;

						if (!block.XX && (block.XY && block.YX && block.YY))
						{
							p1.Set(p + vector3df(0, -BLOCK_SIZE, BLOCK_SIZE));
							p2.Set(p);
							p3.Set(p + vector3df(BLOCK_SIZE, -BLOCK_SIZE, 0));
						}
						else if (!block.XY && (block.XX && block.YX && block.YY))
						{
							p1.Set(p + vector3df(0, -BLOCK_SIZE, 0));
							p2.Set(p + vector3df(BLOCK_SIZE, 0, 0));
							p3.Set(p + vector3df(BLOCK_SIZE, -BLOCK_SIZE, BLOCK_SIZE));
						}
						else if (!block.YX && (block.XY && block.XX && block.YY))
						{
							p1.Set(p + vector3df(BLOCK_SIZE, -BLOCK_SIZE, BLOCK_SIZE));
							p2.Set(p + vector3df(0, 0, BLOCK_SIZE));
							p3.Set(p + vector3df(0, -BLOCK_SIZE, 0));
						}
						else
						{
							p1.Set(p + vector3df(BLOCK_SIZE, -BLOCK_SIZE, 0));
							p2.Set(p + vector3df(BLOCK_SIZE, 0, BLOCK_SIZE));
							p3.Set(p + vector3df(0, -BLOCK_SIZE, BLOCK_SIZE));
						}

						if (PointInTriangle(pos, p1, p2, p3))
							return GetHeightOnTriangle(pos, p1, p2, p3);
						else
							continue;
					}

					// if the block has any raised corners we'll need to get the height on the face
					vector3df p1(p + vector3df(0, -(GLfloat)block.XX * BLOCK_SIZE, 0));
					vector3df p2(p + vector3df(BLOCK_SIZE, -(GLfloat)block.XY * BLOCK_SIZE, 0));
					vector3df p3(p + vector3df(0, -(GLfloat)block.YX * BLOCK_SIZE, BLOCK_SIZE));
					vector3df p4(p + vector3df(BLOCK_SIZE, -(GLfloat)block.YY * BLOCK_SIZE, BLOCK_SIZE));
					bool bFlipped = block.XY + block.YX > block.XX + block.YY;

					if (bFlipped)
					{
						if (PointInTriangle(pos, p1, p2, p4))
							return GetHeightOnTriangle(pos, p1, p2, p4);
						else
							return GetHeightOnTriangle(pos, p4, p3, p1);
					}
					else
					{
						if (PointInTriangle(pos, p1, p2, p3))
							return GetHeightOnTriangle(pos, p1, p2, p3);
						else
							return GetHeightOnTriangle(pos, p2, p3, p4);
					}
				}
			}

			y += CHUNK_SIZE;
			chunkPos.y--;

			if (m_Terrain.find(chunkPos) == m_Terrain.end())
				return 0;

			pChunk = m_Terrain.at(chunkPos);

			if (!pChunk)
				return 0;
		}

		// if we were unsuccessful, return 0
		return 0;
	}
	else
	{
		// a chunk doesn't exist here, just return 0
		return 0;
	}
}

GLfloat TerrainManager::GetDistanceAbove(vector3df pos) const
{
	// make sure that a chunk exists in this location
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// the chunk exists
		int x = blockPos.x;
		int y = blockPos.y;
		int z = blockPos.z;

		//bool searching = true;
		int height = (int)(pos.y * ONE_OVER_BLOCK_SIZE);
		if (pos.y < 0) height--;
		while (true)
		{
			for (; y < CHUNK_SIZE; y++, height++) {
				if (pChunk->terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y].Solid)
					return (int)(height * BLOCK_SIZE) - pos.y;
			}

			y = 0;
			chunkPos.y++;

			if (m_Terrain.find(chunkPos) == m_Terrain.end())
				return 9001;

			pChunk = m_Terrain.at(chunkPos);

			if (!pChunk)
				return 9001;
		}

		// if we were unsuccessful, return 0
		return 9001;
	}
	else
	{
		// a chunk doesn't exist here, just return 0
		return 9001;
	}
}

Biome TerrainManager::GetBiome(const vector3df& pos)
{
	// get the biome at the given point
	vector2di _pos(pos.x / BLOCK_SIZE, pos.z / BLOCK_SIZE);
	if (pos.x < 0) --_pos.x;
	if (pos.z < 0) --_pos.y;
	vector2di sectorPos;
	vector<int>* sector = GetSector(_pos, sectorPos);
	vector2di _where;
	GetPositionInSector(_pos, sectorPos, _where);

	return m_Biomes[sector->at(_where.x * m_SectorSize + _where.y)];
}

Biome TerrainManager::GetBiome(const vector3di& chunkPos, const vector3di& blockPos)
{
	// get the biome at the given point
	vector3di cpos(chunkPos), bpos(blockPos);
	PositionCorrect(bpos, cpos);
	vector2di _pos(cpos.x * CHUNK_SIZE + bpos.x, cpos.z * CHUNK_SIZE + bpos.z);
	vector2di sectorPos;
	vector<int>* sector = GetSector(_pos, sectorPos);
	vector2di _where;
	GetPositionInSector(_pos, sectorPos, _where);

	return m_Biomes[sector->at(_where.x * m_SectorSize + _where.y)];
}

void TerrainManager::SetBiome(const vector3di& chunkPos, const vector3di& blockPos, const Biome& biome)
{
	// get the biome at the given point
	vector3di cpos(chunkPos), bpos(blockPos);
	PositionCorrect(bpos, cpos);
	vector2di _pos(cpos.x * CHUNK_SIZE + bpos.x, cpos.z * CHUNK_SIZE + bpos.z);
	vector2di sectorPos;
	vector<int>* sector = GetSector(_pos, sectorPos);
	vector2di _where;
	GetPositionInSector(_pos, sectorPos, _where);

	sector->at(_where.x * m_SectorSize + _where.y) = biome.index;
}

vector<int>* TerrainManager::GetSector(const vector2di& pos, vector2di& out)
{
	// chunks in sectors: a sector is a group of 64 x 64 chunks, meaning a "island" generation covers 1024 blocks
	// these are set by default although will be changeable for bigger / smaller islands
	
	// check to see if there is a sector generated for this position already
	out = pos;
	int add = (m_ChunkLengthPerSector / 2) * CHUNK_SIZE;
	out += vector2di(add, add);
	out /= (m_ChunkLengthPerSector * CHUNK_SIZE);
	if (pos.x < -add)
		out.x--;
	if (pos.y < -add)
		out.y--;

	map<vector2di, vector<int>>::iterator it = m_BiomeMap.find(out);
	if (it != m_BiomeMap.end())
		return &it->second;
	else
	{
		// there is no sector generated here yet, time to generate a new sector
		vector<int> biome(m_SectorSize * m_SectorSize);
		PerlinNoise noise(m_TerrainSeed);
		struct Point {
			vector2di pos;
			bool land;
			int biome;
		};
		vector<Point> points;
		srand(m_TerrainSeed);
		int num = 200;

		for (int i = 0; i < num; i++) {
			// get a random point to create a voronoi point at
			int x = rand() % m_SectorSize;
			int y = rand() % m_SectorSize;

			// determine if this is land
			double val = noise.GetNoise(x * 0.1, y * 0.1, 0.25);
			double length = vector2di(m_SectorSize / 2, m_SectorSize / 2).Distance(vector2di(x, y)) / m_SectorSize * 2.8;

			// place the point here
			Point point;
			point.land = (val > 0.3 + 0.3 * length * length);
			point.pos = vector2di(x, y);
			point.biome = point.land + rand() % (m_Biomes.size() - 1);
			points.push_back(point);
		}

		// function for calculating (by brute-force) voronoi
		auto voronoi = [&points, &num](int x, int y, int& biome) -> bool {
			bool ret = false;
			double _dist = 99999999;
			for (int i = 0; i < num; i++) {
				double tmp = vector2di(x, y).Distance(points[i].pos);
				if (tmp < _dist)
				{
					_dist = tmp;
					ret = points[i].land;
					biome = points[i].biome;
				}
			}

			return ret;
		};

		// fill out our sector map with biome values
		for (int x = 0; x < m_SectorSize; x++) {
			for (int y = 0; y < m_SectorSize; y++) {
				int n = 0;

				if (voronoi(x, y, n))
					biome[x * m_SectorSize + y] = n; // land biome
				else
					biome[x * m_SectorSize + y] = 0; // ocean biome
			}
		}

		// push back the biome and return a pointer to it
		m_BiomeMap.emplace(out, biome);
		return &m_BiomeMap.at(out);
	}
}

void TerrainManager::GetPositionInSector(const vector2di& pos, const vector2di& sectorPos, vector2di& out) const
{
	// convert the pos input to a position in the sector (to use for getting biome at a given point)
	out = pos;
	int add = (m_ChunkLengthPerSector / 2) * CHUNK_SIZE;
	out += vector2di(add, add);
	out -= vector2di(sectorPos) * m_ChunkLengthPerSector * CHUNK_SIZE;
	out /= (m_SectorSize / CHUNK_SIZE);
}

void TerrainManager::InventoryToData(vector<Item*>* inv, vector<unsigned char>& out)
{
	// union for conversion
	union {
		short size;
		unsigned char byte[2];
	} conv;
	
	// put this inventory into the given data vector
	for (GLuint i = 0; i < inv->size(); i++) {
		if (inv->at(i))
		{
			// there is an item here, write the position
			out.push_back(i);

			// write the length of the name
			conv.size = inv->at(i)->GetName().length();
			out.push_back(conv.byte[0]);
			out.push_back(conv.byte[1]);

			// write the name with a null terminator at the end
			for (GLuint j = 0; j < conv.size; j++) {
				out.push_back((unsigned char)inv->at(i)->GetName()[j]);
			}

			// push back the stack size
			conv.size = inv->at(i)->GetStackSize();
			out.push_back(conv.byte[0]);
			out.push_back(conv.byte[1]);
		}
	}
}

int TerrainManager::GetItemCount(vector<Item*>* inv)
{
	// get the number of non-NULL items in the given inventory - MUST BE A VALID INVENTORY
	int ret = 0;
	for (GLuint i = 0; i < inv->size(); i++) {
		if (inv->at(i))
			ret++;
	}

	return ret;
}

bool TerrainManager::LoadChunk(const vector3di& chunkPos)
{
	// attempt to load this chunk from the disk, if it exists and is loaded then return true, otherwise return false
	// figure out what group we are in
	vector3di groupPos = GetGroupPos(chunkPos);

	// get the name of the chunk group file
	string filename = GroupFileName(groupPos).append(".dat");

	// read the file contents
	unsigned char* buf = new unsigned char[CHUNK_BUFFER_SIZE];
	int len;
	if (!ReadDataFile(filename, chunkPos, buf, len))
	{
		delete[] buf;
		return false;
	}

	// uncompress the data
	bool bSuccess = true;
	uLongf size = CHUNK_BUFFER_SIZE;
	uncompress(reinterpret_cast<Bytef*>(m_Terrain.at(chunkPos)->terrain), &size, buf, len);

	// if the data was all 0 then return false and re-generate the chunk
	Bytef* zero_buffer = new Bytef[CHUNK_BUFFER_SIZE];
	memset(zero_buffer, 0, sizeof(zero_buffer));
	if (memcmp(reinterpret_cast<Bytef*>(m_Terrain.at(chunkPos)->terrain), zero_buffer, sizeof(zero_buffer)) == 0)
	{
		bSuccess = false;
		_out << "Chunk corruption!" << _end;
	}

	// clean up
	delete[] buf;
	delete[] zero_buffer;
	return bSuccess;
}

bool TerrainManager::LoadWater(const vector3di& chunkPos)
{
	// attempt to load this chunk from the disk, if it exists and is loaded then return true, otherwise return false
	// figure out what group we are in
	vector3di groupPos = GetGroupPos(chunkPos);

	// get the name of the chunk group file
	string filename = GroupFileName(groupPos).append(".wtd");

	// read the file contents
	unsigned char* buf = new unsigned char[CHUNK_WATER_BUFFER_SIZE];
	int len;
	if (!ReadDataFile(filename, chunkPos, buf, len))
	{
		delete[] buf;
		return false;
	}

	// uncompress the data
	uLongf size = CHUNK_WATER_BUFFER_SIZE;
	uncompress(reinterpret_cast<Bytef*>(m_Terrain.at(chunkPos)->currentWater), &size, buf, len);
	memcpy(m_Terrain.at(chunkPos)->newWater, m_Terrain.at(chunkPos)->currentWater, size);

	// clean up
	delete[] buf;
	return true;
}

bool TerrainManager::LoadMultiblockLists(const vector3di& chunkPos)
{
	// attempt to load the multiblock lists for this chunk from the disk, if it exists and is loaded then return true, otherwise return false
	// figure out what group we are in
	vector3di groupPos = GetGroupPos(chunkPos);

	// get the name of the chunk group file
	string filename = GroupFileName(groupPos, "chunkdetail").append(".mbl");

	// read the file contents
	unsigned char* buf = new unsigned char[CHUNK_BUFFER_SIZE];
	int len;
	if (!ReadDataFile(filename, chunkPos, buf, len))
	{
		delete[] buf;
		return false;
	}

	// uncompress the data
	uLongf size = CHUNK_BUFFER_SIZE;
	unsigned char* data = new unsigned char[size];
	memset(data, 0, size);
	uncompress(data, &size, buf, len);

	// get the terrain chunk at this position
	TerrainChunk* chunk = m_Terrain.at(chunkPos);

	// loop through the uncompressed data and create the multiblock lists for this chunk
	for (uLongf i = 0; i < size; ) {
		// start by reading the length of this list
		char length = static_cast<char>(data[i]);

		// create a new list
		std::list<vector3di>* list = new std::list<vector3di>;

		// now go through and extract position information for this multi block list
		for (GLuint u = i + 1; u < i + 1 + length * 3; u += 3) {
			int x = static_cast<int>(data[u]);
			int y = static_cast<int>(data[u + 1]);
			int z = static_cast<int>(data[u + 2]);

			list->push_back(vector3di(x, y, z));
			chunk->multiBlockLists.emplace(vector3di(x, y, z), list);
		}

		i += length * 3 + 1;
	}

	// clean up
	delete[] buf;
	delete[] data;
	return true;
}

bool TerrainManager::LoadBlockDetails(const vector3di& chunkPos)
{
	// attempt to load the block details for this chunk from the disk, if it exists and is loaded then return true, otherwise return false
	// figure out what group we are in
	vector3di groupPos = GetGroupPos(chunkPos);

	// get the name of the chunk group file
	string filename = GroupFileName(groupPos, "chunkdetail").append(".bld");

	// read the file contents
	unsigned char* buf = new unsigned char[CHUNK_BUFFER_SIZE];
	int len;
	if (!ReadDataFile(filename, chunkPos, buf, len))
	{
		delete[] buf;
		return false;
	}

	// uncompress the data
	uLongf size = CHUNK_BUFFER_SIZE;
	unsigned char* data = new unsigned char[size];
	memset(data, 0, size);
	uncompress(data, &size, buf, len);

	// get the terrain chunk at this position
	TerrainChunk* chunk = m_Terrain.at(chunkPos);

	// loop through the uncompressed data and create the block details for this chunk
	for (uLongf i = 0; i < size; ) {
		// start by reading the position of these block details
		int x = static_cast<int>(data[i]);
		int y = static_cast<int>(data[i + 1]);
		int z = static_cast<int>(data[i + 2]);
		vector3di blockPos(x, y, z);

		// then read the size of the inventory
		int invSize = static_cast<int>(data[i + 3]);

		// then read the number of saved items
		int num = static_cast<int>(data[i + 4]);

		// create a new block details struct and give it an inventory
		BlockDetails details;
		details.count = 0;

		vector<Item*>* pInv = new vector<Item*>;
		for (GLuint i = 0; i < invSize; i++) {
			pInv->push_back(NULL);
		}
		details.inventory = pInv;
		details.count = -1;

		// read in the item data
		GLuint pos = i + 5;
		for (GLuint u = 0; u < num; u++) {
			// read the cell
			int cell = data[pos];

			// read the length of the name
			short length;
			ByteToShort(&data[pos + 1], length);

			// read in the name
			std::stringstream name;
			GLuint i = 0;
			while (i < length)
			{
				name << (char)data[pos + 3 + i];
				i++;
			}

			// read in the stack size
			short stacksize;
			ByteToShort(&data[pos + 3 + length], stacksize);

			// create the item
			details.inventory->at(cell) = new Item(name.str(), stacksize);

			// increase position
			pos += length + 5;
		}

		// place it in the map
		chunk->blockDetails.emplace(blockPos, details);

		// move along
		i += pos;
	}

	// clean up
	delete[] buf;
	delete[] data;
	return true;
}

void TerrainManager::GenerateTerrain(const vector3di& pos, bool reset)
{
	// get the chunk
	TerrainChunk* c = m_Terrain.at(pos);
	if (c)
	{
		// this should always be reached but as a safety precaution the check will be left in
		// firstly let us try and load the terrain chunk from the disk if it exists
		if (!reset && LoadChunk(pos))
		{
			LoadWater(pos);
			LoadMultiblockLists(pos);
			LoadBlockDetails(pos);
			c->bGenerated = true;
			c->bNeedsGeneration = false;
			c->bWaterSettled = true;
			return;
		}

		// if this is a new chunk then generate new data
		vector2di noisepos(pos.x, pos.z);
		if (m_TerrainNoise.find(noisepos) == m_TerrainNoise.end())
		{
			// we need to generate the height map for this chunk's xz position
			std::vector<GLfloat> n;
			std::vector<int> b;
			GenerateNoise(noisepos, n, b);
			m_TerrainNoise.emplace(noisepos, n);
			m_TerrainBiomes.emplace(noisepos, b);
		}

		std::vector<GLfloat>& n = m_TerrainNoise.at(noisepos);
		std::vector<int>&b = m_TerrainBiomes.at(noisepos);

		// now generate the terrain based on the biome and heightmap
		auto noise_height = [&](int x, int z) -> GLfloat {
			return n[CHUNK_SIZE + 3 + (x * (CHUNK_SIZE + 2) + z)];
		};

		auto biome_at = [&](int x, int z) -> Biome {
			return m_Biomes[b[CHUNK_SIZE + 3 + (x * (CHUNK_SIZE + 2) + z)]];
		};

		PerlinNoise _n(m_TerrainSeed);
		static TerrainBlock space(0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0);
		GLfloat height = 0;

		auto get_layer = [&](const Biome& biome, GLint y) -> const layer* {
			// check if the layers are empty
			if (biome.layers.empty())
				return NULL;

			// get the highest layer and check if we're above that
			if (y > (--biome.layers.end())->first)
				return NULL;

			// get the lowest layer
			int _where = biome.layers.begin()->first;

			// loop through the rest of the layers until we find one higher than _where
			std::map<int, layer>::const_iterator it = --biome.layers.end();
			while (true)
			{
				if (it->first >= y)
					_where = it->first;

				if (it != biome.layers.begin())
					it--;
				else
					break;
			}

			// return the layer
			return &biome.layers.at(_where);
		};

		/*auto smooth_biome = [&](const vector3di& chunkPos, const vector3di& blockPos, Biome& _biome) {
			for (GLint i = 0; i < CHUNK_SIZE; i++) {
				for (GLint j = 0; j < CHUNK_SIZE; j++) {
					GLfloat val = 0;
					GLfloat div = 1;
					int count = 0;
					Biome& biome = GetBiome(chunkPos, blockPos);
					Biome other;

					for (GLint x = max(i - 2, 0); x <= min(i + 2, CHUNK_SIZE); x++) {
						for (GLint y = max(j - 2, 0); y <= min(j + 2, CHUNK_SIZE); y++) {
							other = GetBiome(chunkPos, vector3di(x, 0, y));

							if (other.name != biome.name)
							{
								count++;
								val += noise_height(x, y) * 0.5f;
							}

							div++;
							val += noise_height(x, y);
						}
					}

					if (count > 0)
					{
						val /= div;
						height = val;

						if (rand() % 9 < count)
							_biome = other;
					}
				}
			}
		};*/

		auto get_gen_list = [&](vector<object_gen>& out, int depth, const Biome& biome) {
			for (GLuint i = 0; i < biome.misc.size(); i++) {
				if (biome.misc.at(i).depth == depth)
					out.push_back(biome.misc.at(i));
			}
		};

		auto get_skylight = [&](const vector3di& _blockPos) -> int {
			vector3di blockPos(_blockPos), chunkPos(pos);
			PositionCorrect(blockPos, chunkPos);

			TerrainBlock b = _GetBlock(blockPos, chunkPos);

			if (b.BlockType == 0 || b.Ignored)
				return b.SkyLight > 0 ? b.SkyLight - 1 : 0;

			return 0;
		};

		// a list of biomes in this chunk, used for ore generation
		vector<Biome> biomes;

		GLint y = pos.y * CHUNK_SIZE;
		for (GLint i = 0; i < CHUNK_SIZE; i++) {
			for (GLint j = 0; j < CHUNK_SIZE; j++) {
				// get the height at this point
				height = noise_height(i, j);

				// get the biome
				Biome biome = biome_at(i, j);

				// see if we need to add this to the biomes list
				if (std::find(biomes.begin(), biomes.end(), biome) == biomes.end())
					biomes.push_back(biome);

				for (GLint k = 0; k < CHUNK_SIZE; k++) {
					// if this is a cave, continue
					if (IsCave(pos, i, k, j) && !(biome.index == 0 && k + y >= -5))
					{
						TerrainBlock block = space;
						block.SkyLight = 0;

						// get light around us if we're on the edge
						if (j == 0) block.SkyLight = get_skylight(vector3di(i, j - 1, k));
						if (j == CHUNK_SIZE - 1 && block.SkyLight == 0) block.SkyLight = get_skylight(vector3di(i, j + 1, k));
						if (i == 0 && block.SkyLight == 0) block.SkyLight = get_skylight(vector3di(i - 1, j, k));
						if (i == CHUNK_SIZE - 1 && block.SkyLight == 0) block.SkyLight = get_skylight(vector3di(i + 1, j, k));
						if (k == 0 && block.SkyLight == 0) block.SkyLight = get_skylight(vector3di(i, j, k - 1));
						if (k == CHUNK_SIZE - 1 && block.SkyLight == 0) block.SkyLight = get_skylight(vector3di(i, j, k + 1));

						// sunlight
						if (block.SkyLight > 0)
						{
							SunlightNode sunNode(vector3di(i, j, k), pos, block.SkyLight);
							m_SunLightCreation.push(sunNode);
						}

						// calculate light based on blocks above us (not 100% accurate but good enough for now)
						if (k + y < height)
						{
							//block.SkyLight = max(0, 15 - (height - (k + y)));
							/*float dist = GetDistanceAbove(vector3df(
								(pos.x * CHUNK_SIZE + i) * BLOCK_SIZE + 5.f,
								(pos.y * CHUNK_SIZE + k) * BLOCK_SIZE + 5.f,
								(pos.z * CHUNK_SIZE + j) * BLOCK_SIZE + 5.f
								));

							if (dist < 9001)
							{
								block.SkyLight = max(min(15 - static_cast<int>(dist * ONE_OVER_BLOCK_SIZE), 15), 0);
							}*/
						}
						
						c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k] = block;
						continue;
					}

					// get the y position to use to compare depth
					GLint _where = (y + k) - (GLint)height;

					// firstly see if there are any misc generations to create
					vector<object_gen> gen;
					get_gen_list(gen, _where, biome);
					if (!gen.empty())
					{
						// there are things that can be generated at this layer, roll the dice
						int chance = rand() % 10000;

						// using this chance, go through the possible generations and see which generation with the lowest chance will spawn
						vector<object_gen>::iterator it = gen.begin();
						while (it != gen.end())
						{
							if (chance > (*it).chance)
								it = gen.erase(it);
							else
								it++;
						}

						// check there are still some generations left in the list
						if (!gen.empty())
						{
							// choose a random one
							int which = rand() % gen.size();
							
							// now spawn a thing at this point!
							const ItemClass* ic = Item::GetItemClass(gen.at(which).index);
							TerrainBlock block(ic->sValues[0]);

							// set properties of the block
							block.Solid = ic->bSolid;
							block.Ignored = ic->bBlockHasMesh || ic->sValues[3];
							if (block.Ignored)
								block.SkyLight = 15;

							// set the block
							c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k] = block;

							// check for a multi-block structure
							if (ic->BlockSize != vector3di(1))
							{
								// we need to create a multi-block list
								block.Multi = 1;

								std::list<vector3di>* list = new std::list<vector3di>;

								list->push_back(vector3di(i, k, j));
								c->multiBlockLists.emplace(vector3di(i, k, j), list);

								for (int _x = 0; _x < ic->BlockSize.x; _x++) {
									for (int _z = 0; _z < ic->BlockSize.z; _z++) {
										for (int _y = 0; _y < ic->BlockSize.y; _y++) {
											// don't do the 0,0,0 block
											if (_x == 0 && _y == 0 && _z == 0)
												continue;

											// ensure there is a chunk here
											TerrainChunk* chunk = c;
											vector3di bpos(_x + i, _y + k, _z + j);
											vector3di cpos(pos);
											PositionCorrect(bpos, cpos);

											if (m_Terrain.find(cpos) == m_Terrain.end())
											{
												// create a chunk here, it will be fully generated at a later time (when being sent to a client)
												chunk = new TerrainChunk;
												m_Terrain.emplace(cpos, chunk);
											}
											else
												chunk = m_Terrain.at(cpos);
											
											chunk->terrain[bpos.x * CHUNK_SIZE * CHUNK_SIZE + bpos.z * CHUNK_SIZE + bpos.y] = block;

											list->push_back(vector3di(_x + i, _y + k, _z + j));
											c->multiBlockLists.emplace(vector3di(_x + i, _y + k, _z + j), list);
										}
									}
								}
							}

							// we will do no other processing, move onto the next block to generate
							continue;
						}
					}

					// check if there is a block existing here already
					if (c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k].BlockType > 0)
						continue;

					// if we are not creating a generation here, generate normal terrain
					const layer* _layer = get_layer(biome, _where);
					if (_layer)
					{
						TerrainBlock block(Item::GetItemClass(_layer->block)->sValues[0]);
						TerrainBlock block2(Item::GetItemClass(_layer->block)->sValues[0]);

						bool bSmoothed = false;
						bool bHalf = false;
						if (_layer->smooth && k < CHUNK_SIZE - 1 && _where == 0)
						{
							//TerrainBlock block2(Item::GetItemClass(_layer->block)->sValues[0]);
							if (k + y < noise_height(i - 1, j - 1) - 1 && !(k + y > noise_height(i - 1, j) - 1 && k + y > noise_height(i, j - 1) - 1))
								block2.XX = 1;
							if (k + y < noise_height(i + 1, j - 1) - 1 && !(k + y > noise_height(i + 1, j) - 1 && k + y > noise_height(i, j - 1) - 1))
								block2.XY = 1;
							if (k + y < noise_height(i - 1, j + 1) - 1 && !(k + y > noise_height(i - 1, j) - 1 && k + y > noise_height(i, j + 1) - 1))
								block2.YX = 1;
							if (k + y < noise_height(i + 1, j + 1) - 1 && !(k + y > noise_height(i + 1, j) - 1 && k + y > noise_height(i, j + 1) - 1))
								block2.YY = 1;

							// we can't have a block with all 4 set, so if this is true, reset them all
							if (block2.XX + block2.XY + block2.YX + block2.YY > 0)
							{
								// flip the bits
								block2.XX = !block2.XX;
								block2.XY = !block2.XY;
								block2.YX = !block2.YX;
								block2.YY = !block2.YY;
								block2.SkyLight = 15;

								if (block2.XX + block2.XY + block2.YX + block2.YY == 3)
								{
									// this is a corner slope, set it to a half-block so that only one triangle is rendered
									block2.Half = 1;
									bHalf = true;
								}

								// if we have one corner set down, set the adjacent two down also
								/*if (block2.XX + block2.XY + block2.YX + block2.YY == 1)
								{
									if (block2.XX)
									{
										block2.XY = 1;
										block2.YX = 1;
									}
									else if (block2.XY)
									{
										block2.XX = 1;
										block2.YY = 1;
									}
									else if (block2.YX)
									{
										block2.XX = 1;
										block2.YY = 1;
									}
									else if (block2.YY)
									{
										block2.XY = 1;
										block2.YX = 1;
									}
								}*/

								c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k + 1] = block2;

								bSmoothed = true;
							}
						}

						if (biome.index == 0 && k + y > -5 && k + y < 1)
						{
							// this is an ocean biome, add some water in
							block.Water = 1;

							c->currentWater[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k] = 250;
							c->newWater[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k] = 250;
							c->bWaterSettled = true;
						}

						if (bSmoothed && !bHalf)
						{
							// the block above us has been smoothed so change us
							const layer* _layer2 = get_layer(biome, _where - 1);
							if (_layer2)
								block = TerrainBlock(Item::GetItemClass(_layer2->block)->sValues[0]);

							block.Leaked = 1;
						}
						else if (bHalf)
						{
							// the block above has been smoothed but we also need to smooth ourself
							if (block2.XX && block2.YY)
							{
								if (block2.XY && k + y > noise_height(i + 1, j) && k + y > noise_height(i, j - 1))
									block.XY = 1;
								else if (k + y > noise_height(i - 1, j) && k + y > noise_height(i, j + 1))
									block.YX = 1;
							}
							else if (block2.XY && block2.YX)
							{
								if (block2.XX && k + y > noise_height(i - 1, j) && k + y > noise_height(i, j - 1))
									block.XX = 1;
								else if (k + y > noise_height(i + 1, j) && k + y > noise_height(i, j + 1))
									block.YY = 1;
							}
							block.SkyLight = 15;
						}

						c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k] = block;

						if (k > 0)
						{
							c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k - 1].XX = 0;
							c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k - 1].XY = 0;
							c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k - 1].YX = 0;
							c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k - 1].YY = 0;
						}
					}
					else
					{
						TerrainBlock block = space;
						block.SkyLight = 15;
						
						if (biome.index == 0 && k + y > -5 && k + y < 1)
						{
							// this is an ocean biome, add some water in
							block.Water = 1;
							block.SkyLight = 0;

							c->currentWater[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k] = 250;
							c->newWater[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k] = 250;
							c->bWaterSettled = true;
						}
						else
						{
							// sunlight
							SunlightNode sunNode(vector3di(i, j, k), pos, 15);
							m_SunLightCreation.push(sunNode);
						}
						
						c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + k] = block;

						continue;
					}
				}
			}
		}

		// do any ore spawns
		if (GetChunkPercentageSpace(pos) < 0.95)
		{
			for (GLuint i = 0; i < biomes.size(); i++) {
				const Biome& biome = biomes.at(i);
				if (!biome.ores.empty())
				{
					for (GLuint j = 0; j < biome.ores.size(); j++) {
						const ore_spawn& ore = biome.ores.at(j);
						int count = rand() % (ore.max + 1);
						for (GLuint k = 0; k < count; k++) {
							// pick a random start point in the chunk
							vector2di start(rand() % CHUNK_SIZE, rand() % CHUNK_SIZE);
							int height = static_cast<int>(noise_height(start.x, start.y));
							int depth = ore.upper + rand() % (ore.lower - ore.upper);
							vector3di _where(start.x, height - depth, start.y);
							int n = 0;

							// create an ore deposit of the given density
							while (n < ore.density)
							{
								// ensure we are in a chunk (this can spill into other chunks)
								vector3di cpos(pos), bpos(_where);
								PositionCorrect(bpos, cpos);
								TerrainChunk* chunk = c;

								if (m_Terrain.find(cpos) == m_Terrain.end())
								{
									// create a chunk here, it will be fully generated at a later time (when being sent to a client)
									chunk = new TerrainChunk;
									m_Terrain.emplace(cpos, chunk);
								}
								else
									chunk = m_Terrain.at(cpos);

								// if there is already an ore of this type here, move along
								TerrainBlock block(Item::GetItemClass(ore.index)->sValues[0]);
								if (chunk->terrain[bpos.x * CHUNK_SIZE * CHUNK_SIZE + bpos.z * CHUNK_SIZE + bpos.y].BlockType != block.BlockType)
								{
									if (chunk->terrain[bpos.x * CHUNK_SIZE * CHUNK_SIZE + bpos.z * CHUNK_SIZE + bpos.y].BlockType > 0 &&
										!chunk->terrain[bpos.x * CHUNK_SIZE * CHUNK_SIZE + bpos.z * CHUNK_SIZE + bpos.y].Multi)
										chunk->terrain[bpos.x * CHUNK_SIZE * CHUNK_SIZE + bpos.z * CHUNK_SIZE + bpos.y] = block;

									n++;
								}

								// move to another block
								_where += vector3di(-1 + rand() % 3, -(rand() % 2), -1 + rand() % 3);
							}
						}
					}
				}
			}
		}

		// update sunlight now
		UpdateSunlightCreation();

		/*for (GLint i = 0; i < CHUNK_SIZE; i++) {
			for (GLint j = 0; j < CHUNK_SIZE; j++) {
				height = noise_height(i, j);

				// see if we want to generate a tree here
				if (height > 3 && height - pos.y * CHUNK_SIZE > 0 && height - pos.y * CHUNK_SIZE < CHUNK_SIZE)
				{
					if (_n.GetNoise(i, j, height + 0.2f) < 0.26)
						c->terrain[i * CHUNK_SIZE * CHUNK_SIZE + j * CHUNK_SIZE + (GLint)height + 1] = bush;
					if (_n.GetNoise(i, j, height) < 0.27)
						_GenerateTree(c, i, height, j);
				}
			}
		}*/

		// set the generated flag
		c->bGenerated = true;
		c->bNeedsGeneration = false;
	}
}

bool TerrainManager::IsCave(const vector3di& chunkPos, int x, int y, int z)
{
	// determine whether the block in this position is part of a cave
	//x += chunkPos.x * CHUNK_SIZE;
	//y += chunkPos.y * CHUNK_SIZE;
	//z += chunkPos.z * CHUNK_SIZE;

	double yVariance = static_cast<double>(y + chunkPos.y * CHUNK_SIZE);
	double _x = static_cast<double>(x + chunkPos.x * CHUNK_SIZE) / static_cast<double>(8);
	double _y = yVariance / static_cast<double>(8);
	double _z = static_cast<double>(z + chunkPos.z * CHUNK_SIZE) / static_cast<double>(8);

	double noise = m_Noise.GetNoise(_x, _z, _y);

	yVariance *= 0.002;
	yVariance += 0.08;

	return (noise <= 0.3618 - std::max(std::min(yVariance, 0.1), -0.17));
}

void TerrainManager::_GenerateTree(TerrainChunk* c, GLint x, GLint y, GLint z)
{
	// decide on a height
	y++;

	TerrainBlock tree(Item::GetBlockFromName("Tree"));
	tree.SkyLight = 15;
	tree.RotY = (x + y + z) % 4;
	tree.Ignored = 1;
	c->terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y] = tree;

	std::list<vector3di>* list = new std::list<vector3di>;

	list->push_back(vector3di(x, y, z));
	c->multiBlockLists.emplace(vector3di(x, y, z), list);
	//AddToMultiBlockList(vector3di(x, y, z), list);

	tree.Multi = 1;
	for (GLuint k = 1; k < 5; k++) {
		if (y + k >= CHUNK_SIZE)
			break;

		//tree.SkyLight = _GetBlock(x, y + k + 1, z).SkyLight;
		c->terrain[x * CHUNK_SIZE * CHUNK_SIZE + z * CHUNK_SIZE + y + k] = tree;

		list->push_back(vector3di(x, y + k, z));
		c->multiBlockLists.emplace(vector3di(x, y + k, z), list);
		//AddToMultiBlockList(vector3di(x, y + k, z), list);
	}
}

void TerrainManager::GenerateNoise(const vector2di& pos, std::vector<GLfloat>& out, std::vector<int>& b)
{
	GLint X = pos.x * CHUNK_SIZE;
	GLint Y = pos.y * CHUNK_SIZE;
	for (GLint i = -1; i <= CHUNK_SIZE; i++) {
		for (GLint j = -1; j <= CHUNK_SIZE; j++) {
			Biome biome = GetBiome(vector3di(pos.x, 0, pos.y), vector3di(i, 0, j));

			if (!biome.funcs.empty())
			{
				double _x = (double)(X + i);
				double _y = (double)(Y + j);
				float val = 0;

				for (GLuint i = 0; i < biome.funcs.size(); i++) {
					height_func& fn = biome.funcs.at(i);
					val += (float)m_Noise.GetNoise(_x * fn.smoothx, _y * fn.smoothy, 0.5) * fn.amplify * fn.mode;
				}

				out.push_back(val);
			}
			else
				out.push_back(0);

			b.push_back(biome.index);
		}
	}
	out.push_back(0);
	b.push_back(GetBiome(vector3di(pos.x, 0, pos.y), vector3di(16, 0, 16)).index);

	// now loop back through the terrain and average any biome borders
	for (GLint i = 0; i < CHUNK_SIZE; i++) {
		for (GLint j = 0; j < CHUNK_SIZE; j++) {
			GLfloat val = 0;
			GLfloat div = 1;
			int count = 0;
			Biome* biome = &m_Biomes[b[CHUNK_SIZE + 3 + (i * (CHUNK_SIZE + 2) + j)]];
			Biome* other = NULL;

			for (GLint x = std::max(i - 2, 0); x <= std::min(i + 2, CHUNK_SIZE); x++) {
				for (GLint y = std::max(j - 2, 0); y <= std::min(j + 2, CHUNK_SIZE); y++) {
					other = &m_Biomes[b[CHUNK_SIZE + 3 + (x * (CHUNK_SIZE + 2) + y)]];

					if (other->index != biome->index)
					{
						count++;
						//val += out[CHUNK_SIZE + 3 + (x * (CHUNK_SIZE + 2) + y)] * 0.5f;
					}
						
					div++;
					val += out[CHUNK_SIZE + 3 + (x * (CHUNK_SIZE + 2) + y)];
				}
			}

			if (count > 0)
			{
				val /= div;
				out[CHUNK_SIZE + 3 + (i * (CHUNK_SIZE + 2) + j)] = val;

				if (rand() % 25 < count && other)
					b[CHUNK_SIZE + 3 + (i * (CHUNK_SIZE + 2) + j)] = other->index;
			}
		}
	}
}

void TerrainManager::GetCornerChunkPosition(const vector3df& pos, vector3di& out)
{
	// get the corner position for the chunk based in a 5x5 grid from point pos
	out.Set((GLint)(pos.x * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.y * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.z * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE));
	if (pos.x < 0) out.x--;
	if (pos.y < 0) out.y--;
	if (pos.z < 0) out.z--;

	out.x -= 2;
	out.y -= 2;
	out.z -= 2;
}

void TerrainManager::SendChunk(vector3di pos, SOCKET& socket, sockaddr_in& client, GLuint counter)
{
	// if we are saving don't send anything
	if (m_bSaving)
		return;
	
	// send chunk information to the given client
	//m_Mutex.lock();
	if (m_Terrain.find(pos) == m_Terrain.end())
	{
		// if the chunk hasn't been generated, generate it now
		TerrainChunk* c = new TerrainChunk;
		c->bNeedsGeneration = true;
		m_Terrain.emplace(pos, c);
		//GenerateTerrain(pos);
		//m_Mutex.unlock();
		return;
	}

	// if the chunk hasn't been generated, generate it now
	if (!m_Terrain.at(pos)->bGenerated)
	{
		//GenerateTerrain(pos);
		m_Terrain.at(pos)->bNeedsGeneration = true;
		//m_Mutex.unlock();
		return;
	}

	// compress the data
	Bytef* comp = new Bytef[COMPRESSED_CHUNK_BUFFER_SIZE];
	Bytef* tdata = reinterpret_cast<Bytef*>(m_Terrain.at(pos)->terrain);
	uLongf size = COMPRESSED_CHUNK_BUFFER_SIZE;
	compress(comp, &size, tdata, CHUNK_BUFFER_SIZE);

	// determine how many packets are needed to send this data
	const int packetsize = 450;
	int packetnum = (size / packetsize) + 1;

	// create the packets and send the data to the client
	int slen = sizeof(client);
	char* data = new char[BUFFER_SIZE];
	data[0] = 0;
	data[1] = terrain_block_data;
	SetBit(data[0], 0, 1);
	UIntToByte(counter, (unsigned char*)&data[2]);

	// chunk position and number of packets
	IntToByte(pos.x, (unsigned char*)&data[HEADER_SIZE]);
	IntToByte(pos.y, (unsigned char*)&data[HEADER_SIZE + 4]);
	IntToByte(pos.z, (unsigned char*)&data[HEADER_SIZE + 8]);
	data[HEADER_SIZE + 12] = packetnum;

	int left = size;
	for (GLuint i = 0; i < packetnum; i++) {
		data[HEADER_SIZE + 13] = i + 1;

		int len = std::min(packetsize, left);
		memcpy(&data[HEADER_SIZE + 14], &comp[i * packetsize], len);
		left -= len;

		sendto(socket, data, len + HEADER_SIZE + 14, 0, (sockaddr*)&client, slen);
	}

	//_out << size << " sent in " << packetnum << " packets" << _end;

	// cleanup
	delete[] comp;
	delete[] data;
	//m_Mutex.unlock();
}

void TerrainManager::SendWater(vector3di pos, SOCKET& socket, sockaddr_in& client, GLuint counter)
{
	// check this chunk exists
	if (m_Terrain.find(pos) == m_Terrain.end())
		return;

	// ensure we are generated
	if (!m_Terrain.at(pos)->bGenerated)
		return;

	// compress the data
	Bytef* comp = new Bytef[COMPRESSED_CHUNK_BUFFER_SIZE];
	Bytef* tdata = reinterpret_cast<Bytef*>(m_Terrain.at(pos)->currentWater);
	uLongf size = COMPRESSED_CHUNK_BUFFER_SIZE;
	compress(comp, &size, tdata, CHUNK_WATER_BUFFER_SIZE);

	// determine how many packets are needed to send this data
	const int packetsize = 450;
	int packetnum = (size / packetsize) + 1;

	// create the packets and send the data to the client
	int slen = sizeof(client);
	char* data = new char[BUFFER_SIZE];
	data[0] = 0;
	data[1] = terrain_water_data;
	SetBit(data[0], 0, 1);
	UIntToByte(counter, (unsigned char*)&data[2]);

	// chunk position and number of packets
	IntToByte(pos.x, (unsigned char*)&data[HEADER_SIZE]);
	IntToByte(pos.y, (unsigned char*)&data[HEADER_SIZE + 4]);
	IntToByte(pos.z, (unsigned char*)&data[HEADER_SIZE + 8]);
	data[HEADER_SIZE + 12] = packetnum;

	int left = size;
	for (GLuint i = 0; i < packetnum; i++) {
		data[HEADER_SIZE + 13] = i + 1;

		int len = std::min(packetsize, left);
		memcpy(&data[HEADER_SIZE + 14], &comp[i * packetsize], len);
		left -= len;

		sendto(socket, data, len + HEADER_SIZE + 14, 0, (sockaddr*)&client, slen);
	}

	//_out << size << " sent in " << packetnum << " packets" << _end;

	// cleanup
	delete[] comp;
	delete[] data;
}

void TerrainBlockToByte(TerrainBlock& in, unsigned char* out)
{
	unsigned char* buf = reinterpret_cast<unsigned char*>(&in);
	memcpy(out, buf, 8);
}

void ByteToTerrainBlock(unsigned char* in, TerrainBlock& out)
{
	unsigned char* buf = reinterpret_cast<unsigned char*>(&out);
	memcpy(buf, in, 8);
}

void TerrainManager::ProcessChangeBlock(unsigned char* buf, int& size)
{
	// uncompress the data received
	uLongf _size = 3000; // (5 * 5 * 5 * 23) rounded up to nearest 1000
	unsigned char* ubuf = new unsigned char[_size];
	uncompress(ubuf, &_size, buf, size - HEADER_SIZE - 1);
	
	// process a block_change message (as sent from above) and change the terrain
	int pos = 0;
	vector3di startpos, startchunkpos;
	TerrainChunk* startchunk = NULL;
	TerrainBlock startblock;
	std::list<vector3di>* pList = NULL;
	vector<std::pair<vector3di, vector3di>> blockstodelete;

	// function to remove block details
	auto remove_block_details = [&](TerrainChunk* pChunk, const vector3di& blockPos) {
		map<vector3di, BlockDetails>::iterator dit = pChunk->blockDetails.find(blockPos);
		if (dit != pChunk->blockDetails.end())
		{
			if (dit->second.inventory)
			{
				// clear this inventory
				for (GLuint i = 0; i < dit->second.inventory->size(); i++) {
					delete dit->second.inventory->at(i);
				}
				delete dit->second.inventory;
			}
			pChunk->blockDetails.erase(blockPos);
		}
	};

	while (pos + 22 <= _size)
	{
		// extract the block data
		TerrainBlock b;
		ByteToTerrainBlock(&ubuf[pos], b);

		// extract the position in chunk
		int x = ubuf[pos + 8];
		int y = ubuf[pos + 9];
		int z = ubuf[pos + 10];
		vector3di blockPos(x, y, z);

		// extract the chunk position
		ByteToInt(&ubuf[pos + 11], x);
		ByteToInt(&ubuf[pos + 15], y);
		ByteToInt(&ubuf[pos + 19], z);
		vector3di chunkPos(x, y, z);

		// debugging
		//_out << _size << " - " << b.BlockType << " : " << x << ", " << y << ", " << z << _end;

		// set the block at this position
		if (m_Terrain.find(chunkPos) != m_Terrain.end())
		{
			// for multi-block lists
			if (b.BlockType > 0 && !b.Multi)
			{
				startpos.Set(blockPos);
				startchunkpos.Set(chunkPos);
				startblock = b;
				startchunk = m_Terrain.at(chunkPos);
			}

			// get the old block
			TerrainBlock old = m_Terrain.at(chunkPos)->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y];

			if (b.BlockType == 0 && old.Water == 1)
				b.Water == 1;

			// create a light update node so we can update the lighting here later in the update method
			LightUpdateNode node;
			node.blockPos = blockPos;
			node.chunkPos = chunkPos;
			node.old = old;
			m_LightUpdates.push_back(node);

			// check the block item class
			// check whether this block can be ignored
			const ItemClass* ic = Item::GetItemClass(b);
			if (b.BlockType == 0 || (ic && (ic->bBlockHasMesh || ic->sValues[3] || ic->BlockSize != vector3di(1, 1, 1))))
				b.Ignored = 1;
			else
				b.Ignored = 0;

			if (b.Water == 0)
			{
				b.Water = 0;
				m_Terrain.at(chunkPos)->currentWater[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y] = 0.f;
				m_Terrain.at(chunkPos)->newWater[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y] = 0.f;
			}
			
			// terrain chunk exists, process the block change
			m_Terrain.at(chunkPos)->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y] = b;
			remove_block_details(m_Terrain.at(chunkPos), blockPos);
			m_Terrain.at(chunkPos)->bWaterSettled = false;

			// multi-block lists
			if (b.BlockType > 0 && b.Multi && b.BlockType == startblock.BlockType)
			{
				// we are placing a multi-block structure, add it to the correct list
				if (!pList && startchunk)
				{
					// if no list yet exists, create one
					pList = new std::list<vector3di>;
					pList->push_back(startpos);
					startchunk->multiBlockLists.emplace(startpos, pList);
				}

				// correct blockPos for different chunks
				vector3di diff = chunkPos - startchunkpos;
				blockPos += diff * CHUNK_SIZE;

				// add this block to the list
				pList->push_back(blockPos);
				startchunk->multiBlockLists.emplace(blockPos, pList);
			}
			else if (b.BlockType == 0)
			{
				// we are deleting a block, drop the items this block drops
				const ItemClass* oldic = Item::GetItemClass(old);
				if (oldic)
				{
					vector3df dropPos = ChunkBlockPosToVector3df(blockPos, chunkPos);
					dropPos += vector3df(BLOCK_SIZE * 0.5f);
					for (GLuint i = 0; i < oldic->Drops.size(); i++) {
						m_pServer->CreateItem(oldic->Drops[i].first, dropPos, oldic->Drops[i].second);
					}
				}
				
				// we are deleting a block, see if we need to remove it from a multi-block list
				TerrainChunk* chunk = NULL;
				std::list<vector3di>* pList = GetMultiBlockList(blockPos, chunkPos, chunk);

				if (pList)
				{
					// this block is part of a multi-block structure!
					std::list<vector3di>::iterator it = pList->begin();
					while (it != pList->end())
					{
						vector3di pos = *it;
						vector3di cpos(chunkPos);
						RemoveFromMultiBlockList(&chunk->multiBlockLists, pos);

						PositionCorrect(pos, cpos);

						blockstodelete.push_back(std::pair<vector3di, vector3di>(pos, cpos));

						// create a light update node so we can update the lighting here later in the update method
						LightUpdateNode node;
						node.blockPos = pos;
						node.chunkPos = cpos;
						node.old = m_Terrain.at(cpos)->terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y];
						m_LightUpdates.push_back(node);

						m_Terrain.at(cpos)->terrain[pos.x * CHUNK_SIZE * CHUNK_SIZE + pos.z * CHUNK_SIZE + pos.y] = b;
						remove_block_details(m_Terrain.at(cpos), pos);

						it++;
					}

					// delete the list
					delete pList;
				}
			}
		}

		// continue to the next block
		pos += 23;
	}

	// if we have any blocks to delete, reset the buffer and send this new data to the clients
	if (!blockstodelete.empty())
	{
		// clear the memory
		_size *= blockstodelete.size();
		memset(ubuf, 0, _size);

		// loop through each block placing it in the buffer
		int position = 0;
		for (GLuint i = 0; i < blockstodelete.size(); i++) {
			vector3di _b = blockstodelete[i].first;
			vector3di _c = blockstodelete[i].second;
			TerrainChunk* chunk = m_Terrain.at(_c);

			TerrainBlockToBuffer(chunk->terrain[_b.x * CHUNK_SIZE * CHUNK_SIZE + _b.z * CHUNK_SIZE + _b.y], _b, _c, ubuf, position);
		}

		// compress the data
		uLongf csize = BUFFER_SIZE - 3;
		memset(buf, 0, BUFFER_SIZE - 3);
		compress(buf, &csize, ubuf, _size);
		size = csize + HEADER_SIZE + 1;
	}

	// clean up
	delete[] ubuf;
}

bool TerrainManager::ProcessInteraction(unsigned char* buf)
{
	// we have received a process interaction packet, unpack it and edit it if necessary
	vector3di blockPos, chunkPos;

	// client id
	int id = buf[0];

	// load block position in chunk
	blockPos.x = buf[1];
	blockPos.y = buf[2];
	blockPos.z = buf[3];

	// load chunk position in world
	ByteToInt(&buf[4], chunkPos.x);
	ByteToInt(&buf[8], chunkPos.y);
	ByteToInt(&buf[12], chunkPos.z);

	// load open/close switch
	bool open = buf[16] > 0;

	// check the chunk exists
	TerrainMap::iterator it = m_Terrain.find(chunkPos);
	if (it != m_Terrain.end())
	{
		// check the block
		TerrainChunk* pChunk = it->second;
		TerrainBlock b = pChunk->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y];

		// if this is a multi-block block then move to the first block in the list so that only one details will exist
		TerrainChunk* temp = it->second;
		std::list<vector3di>* list = GetMultiBlockList(blockPos, chunkPos, temp);
		if (b.Multi)
		{
			if (!list)
				// something has gone wrong, abort!
				return false;

			blockPos.Set(list->front());

			// correct the data in the buffer
			unsigned char _x = (unsigned char)blockPos.x;
			unsigned char _y = (unsigned char)blockPos.y;
			unsigned char _z = (unsigned char)blockPos.z;
			buf[1] = _x;
			buf[2] = _y;
			buf[3] = _z;

			// place the chunk position in the buffer
			IntToByte(chunkPos.x, &buf[4]);
			IntToByte(chunkPos.y, &buf[8]);
			IntToByte(chunkPos.z, &buf[12]);
		}

		const ItemClass* ic = Item::GetItemClass(b);

		if (ic)
		{
			// if we have a block at this position check if it has an inventory
			bool inv = ic->sValues[4] > 0;

			// check if we have a block details struct at this position
			if (pChunk->blockDetails.find(blockPos) == pChunk->blockDetails.end())
			{
				// this is a new block placed by a client so the server does not yet have any details stored for it
				// create a new block details struct and give it an inventory (if required)
				BlockDetails d;
				d.count = 0;

				if (inv)
				{
					vector<Item*>* pInv = new vector<Item*>;
					for (GLuint i = 0; i < ic->sValues[4]; i++) {
						pInv->push_back(NULL);
					}
					d.inventory = pInv;
					d.count = -1;
				}
				else
					d.inventory = NULL;

				// place it in the map
				pChunk->blockDetails.emplace(blockPos, d);
			}

			// get the details for this block
			BlockDetails& details = pChunk->blockDetails.at(blockPos);

			// now if this has an inventory it will behave differently from interactables with two states
			if (inv)
			{
				// a client is opening a chest or another block with an inventory
				if (open && details.count == -1)
				{
					// increment the count for this detail
					details.count = id;

					// we now need to send an inventory message to the client who opened this chest
					SendInventory(id, pChunk, blockPos);

					// re-send the message as-is
					return true;
				}
				else
				{
					// if the count is the same as the clientid, close the chest
					if (details.count == id)
					{
						details.count = -1;
						return true;
					}
					else
						return false;
				}
			}
			else
			{
				// a client is opening a door, flipping a switch, toggling something...
				// it doesn't really matter what value open has, just toggle the current state
				if (details.count > 0)
				{
					// close the interactable
					details.count = 0;
					buf[16] = 0;
				}
				else
				{
					// open the interactable
					details.count = 1;
					buf[16] = 1;
				}

				// go through and set these blocks to not be solid
				if (b.Multi && list)
				{
					std::list<vector3di>::iterator it = list->begin();
					while (it != list->end())
					{
						vector3di _chunkPos(chunkPos);
						vector3di _blockPos(*it);
						PositionCorrect(_blockPos, _chunkPos);

						TerrainChunk* chunk = m_Terrain.at(_chunkPos);
						chunk->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y].Solid = 1 - details.count;

						it++;
					}
				}
				else
				{
					TerrainChunk* chunk = m_Terrain.at(chunkPos);
					chunk->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y].Solid = 1 - details.count;
				}

				// send the message
				return true;
			}
		}
	}

	// if we return false, no reply packet will be sent to any clients
	return false;
}

void TerrainManager::ProcessBlockInventoryUpdate(unsigned char* buf, int rlen)
{
	// a client has updated an inventory of a block, update it
	vector3di blockPos, chunkPos;

	// client id
	int id = buf[0];

	// load block position in chunk
	blockPos.x = buf[1];
	blockPos.y = buf[2];
	blockPos.z = buf[3];

	// load chunk position in world
	ByteToInt(&buf[4], chunkPos.x);
	ByteToInt(&buf[8], chunkPos.y);
	ByteToInt(&buf[12], chunkPos.z);

	// make sure there are details here and it is an inventory
	if (m_Terrain.at(chunkPos)->blockDetails.find(blockPos) == m_Terrain.at(chunkPos)->blockDetails.end())
		return;

	BlockDetails& details = m_Terrain.at(chunkPos)->blockDetails.at(blockPos);
	if (!details.inventory)
		return;

	// clear the inventory
	for (GLuint i = 0; i < details.inventory->size(); i++) {
		delete details.inventory->at(i);
		details.inventory->at(i) = NULL;
	}

	if (rlen > 16)
	{
		// uncompress the data
		uLongf size = details.inventory->size() * 300 + 300;
		unsigned char* ubuf = new unsigned char[size];
		uncompress(ubuf, &size, &buf[16], rlen - 16);

		// read the items in and create them
		GLuint pos = 0;
		const size_t ilen = sizeof(ItemData);
		while (pos < size)
		{
			// read the cell
			int cell = ubuf[pos];

			// read in the item data
			ItemData data = ItemDataFromBuffer(&ubuf[pos + 1]);

			// create the item
			details.inventory->at(cell) = new Item(data.index, data.amount);

			// increase position
			pos += ilen + 1;
		}

		// clean up
		delete[] ubuf;
	}
}

void TerrainManager::SendInventory(int clientid, TerrainChunk* pChunk, const vector3di& blockPos)
{
	// a client has opened an inventory block (like a chest) and so we need to send the contents of its inventory to the client
	// check the chunk exists
	if (pChunk)
	{
		// check the block
		TerrainBlock b = pChunk->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y];

		// get the details for this block
		BlockDetails& details = pChunk->blockDetails.at(blockPos);

		// the way this message is structured is as follows:
		// (this is a view of the uncompressed data)
		// |[##][i][s]|[n][ i t e m ][n][ i t e m ]...
		// | | - between these two is uncompressed all the time
		// where # = first two reserved bits
		// i = client id
		// s = size of inventory
		// n = position of item in inventory
		// i t e m = details of item, ie name and stack size - ItemData struct
		// get the inventory item details and add the data to a vector
		vector<unsigned char> items;
		vector<Item*>* inv = details.inventory;

		// put this inventory into the given data vector
		for (GLuint i = 0; i < inv->size(); i++) {
			if (inv->at(i))
			{
				// there is an item here, write the position
				items.push_back(i);

				// create some itemdata and push it back
				ItemData data;
				data.index = inv->at(i)->GetIndex();
				data.amount = inv->at(i)->GetStackSize();
				data.pos.Set(0, 0, 0);
				unsigned char* itemdata = ItemDataToBuffer(data);

				for (GLuint j = 0; j < sizeof(ItemData); j++) {
					items.push_back(itemdata[j]);
				}
			}
		}

		if (!items.empty())
		{
			// now we can compress the data
			uLongf size = items.size() + 100;
			unsigned char* cbuf = new unsigned char[size];
			compress(cbuf, &size, items.data(), items.size());

			// we now need to create another buffer to add the first two bytes
			unsigned char* buf = new unsigned char[size + 2];
			buf[0] = clientid;
			buf[1] = inv->size();
			memcpy(&buf[2], cbuf, size);

			// send the packet and clean up
			m_pServer->SendPacket(clientid, block_inventory, buf, size + 2);
			delete[] cbuf;
			delete[] buf;
		}
		else
		{
			// the inventory for this block is empty
			unsigned char* buf = new unsigned char[2];
			buf[0] = clientid;
			buf[1] = inv->size();

			// send the packet
			m_pServer->SendPacket(clientid, block_inventory, buf, 2, true);
			delete[] buf;
		}
	}
}

vector3df TerrainManager::ChunkBlockPosToVector3df(const vector3di& blockPos, const vector3di& chunkPos)
{
	return vector3df((chunkPos.x * CHUNK_SIZE + blockPos.x) * BLOCK_SIZE,
		(chunkPos.y * CHUNK_SIZE + blockPos.y) * BLOCK_SIZE,
		(chunkPos.z * CHUNK_SIZE + blockPos.z) * BLOCK_SIZE);
}

TerrainBlock TerrainManager::_GetBlock(const vector3di& blockPos, const vector3di& chunkPos) const
{
	// get the block at this position
	TerrainChunk* pChunk = GetChunk(chunkPos);
	if (pChunk)
		return pChunk->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y];

	// there is no chunk at this position, return air with sun light = 0
	static TerrainBlock block(0, 0, 0, 0, 0, chunkPos.y >= 0, 0, 0, 0, 0, 0, 0, 0, 0);
	return block;
}

TerrainChunk* TerrainManager::GetChunk(const vector3di& pos) const
{
	// see if a chunk exists at this position
	TerrainMap::const_iterator it = m_Terrain.find(pos);
	if (it != m_Terrain.end())
		return it->second;

	return NULL;
}

void TerrainManager::PositionCorrect(vector3di& blockPos, vector3di& chunkPos)
{
	// function to correct position errors
	auto position_correct = [](int& in1, int& in2) {
		while (in1 < 0)
		{
			in1 += CHUNK_SIZE;
			in2--;
		}
		while (in1 >= CHUNK_SIZE)
		{
			in1 -= CHUNK_SIZE;
			in2++;
		}
	};

	// correct block / chunk positions
	position_correct(blockPos.x, chunkPos.x);
	position_correct(blockPos.y, chunkPos.y);
	position_correct(blockPos.z, chunkPos.z);
}

std::list<vector3di>* TerrainManager::GetMultiBlockList(vector3di block, vector3di& chunk, TerrainChunk*& pChunk)
{
	// define the get_multiblock_list function
	auto get_multiblock_list = [](TerrainChunk* pChunk, const vector3di& pos) -> std::list<vector3di>* {
		if (pChunk->multiBlockLists.find(pos) != pChunk->multiBlockLists.end())
			return pChunk->multiBlockLists.at(pos);

		return NULL;
	};
	
	// we need to find the list with the given block position in
	std::list<vector3di>* list = get_multiblock_list(m_Terrain[chunk], block);
	pChunk = m_Terrain[chunk];

	// if this block's list is actually in another chunk (it will be in an adjacent chunk in the negative direction) then find it
	if (list == NULL)
	{
		vector3di b = block + vector3di(CHUNK_SIZE, 0, 0);
		vector3di p = chunk + vector3di(-1, 0, 0);
		pChunk = m_Terrain[p];

		if (pChunk)
			list = get_multiblock_list(pChunk, b);

		if (!list)
		{
			b = block + vector3di(0, 0, CHUNK_SIZE);
			p = chunk + vector3di(0, 0, -1);
			pChunk = m_Terrain[p];

			if (pChunk)
				list = get_multiblock_list(pChunk, b);

			if (!list)
			{
				b = block + vector3di(0, CHUNK_SIZE, 0);
				p = chunk + vector3di(0, -1, 0);
				pChunk = m_Terrain[p];

				if (pChunk)
					list = get_multiblock_list(pChunk, b);

				if (!list)
				{
					b = block + vector3di(CHUNK_SIZE, 0, CHUNK_SIZE);
					p = chunk + vector3di(-1, 0, -1);
					pChunk = m_Terrain[p];

					if (pChunk)
						list = get_multiblock_list(pChunk, b);

					if (!list)
					{
						b = block + vector3di(CHUNK_SIZE, CHUNK_SIZE, CHUNK_SIZE);
						p = chunk + vector3di(-1, -1, -1);
						pChunk = m_Terrain[p];

						if (pChunk)
							list = get_multiblock_list(pChunk, b);

						if (!list)
						{
							b = block + vector3di(CHUNK_SIZE, CHUNK_SIZE, 0);
							p = chunk + vector3di(-1, -1, 0);
							pChunk = m_Terrain[p];

							if (pChunk)
								list = get_multiblock_list(pChunk, b);

							if (!list)
							{
								b = block + vector3di(0, CHUNK_SIZE, CHUNK_SIZE);
								p = chunk + vector3di(0, -1, -1);
								pChunk = m_Terrain[p];

								if (pChunk)
									list = get_multiblock_list(pChunk, b);

								// ok that should cover all the possibilities so if we haven't found it it doesn't exist
							}
						}
					}
				}
			}
		}

		if (list)
			chunk = p;
	}

	return list;
}

void TerrainManager::RemoveFromMultiBlockList(map<vector3di, std::list<vector3di>*>* pMap, const vector3di& pos)
{
	// remove the given block from the multiblock list
	map<vector3di, std::list<vector3di>*>::iterator it = pMap->find(pos);

	if (it != pMap->end())
	{
		pMap->erase(it);
	}
}

void TerrainManager::TerrainBlockToBuffer(TerrainBlock& in, vector3di& blockPos, vector3di& chunkPos, unsigned char* buf, int& start)
{
	// convert the terrainblock to bytes
	TerrainBlockToByte(in, &buf[start]);

	// place the block position in the chunk
	unsigned char _x = (unsigned char)blockPos.x;
	unsigned char _y = (unsigned char)blockPos.y;
	unsigned char _z = (unsigned char)blockPos.z;
	buf[start + 8] = _x;
	buf[start + 9] = _y;
	buf[start + 10] = _z;

	// place the chunk position in the buffer
	IntToByte(chunkPos.x, &buf[start + 11]);
	IntToByte(chunkPos.y, &buf[start + 15]);
	IntToByte(chunkPos.z, &buf[start + 19]);

	// increase the position
	start += 23;
}

// lighting

TerrainChunk* TerrainManager::SetBlockLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint red, GLuint green, GLuint blue)
{
	// set the block light at this position and push back a new light creation node
	TerrainChunk* ret = _SetBlockLight(blockPos, chunkPos, red, green, blue);
	m_BlockLightCreation.emplace(blockPos, chunkPos, red, green, blue);

	// update light creation
	UpdateLightCreation();

	// return the chunk
	return ret;
}

void TerrainManager::RemoveBlockLight(const vector3di& blockPos, const vector3di& chunkPos, const TerrainBlock& block)
{
	// get the block here already
	m_BlockLightDeletion.emplace(blockPos, chunkPos, (int)block.LightRed, (int)block.LightGreen, (int)block.LightBlue);

	// set the block light here
	_SetBlockLight(blockPos, chunkPos, 0, 0, 0);

	// update light deletion
	UpdateLightDeletion();
}

TerrainChunk* TerrainManager::_SetBlockLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint red, GLuint green, GLuint blue)
{
	// set the block light at the given position
	TerrainChunk *pChunk = GetChunk(chunkPos);
	if (pChunk)
	{
		pChunk->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y].LightRed = red;
		pChunk->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y].LightGreen = green;
		pChunk->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y].LightBlue = blue;
	}

	return pChunk;
}

void TerrainManager::UpdateLightCreation()
{
	// update light creation
	vector3di chunkPos, blockPos;
	GLuint r, g, b;

	// function to compare light levels
	auto light_compare = [](GLuint v, GLuint c) -> bool {
		return v + 2 <= c;
	};

	// function to check a light value at a given point
	auto check_light = [&](vector3di chunkPos, vector3di blockPos) {
		// if this block position is out of this chunk, correct it
		PositionCorrect(blockPos, chunkPos);

		// get the block at this position
		TerrainBlock tb = _GetBlock(blockPos, chunkPos);

		// compare lighting values
		bool _r = light_compare(tb.LightRed, r);
		bool _g = light_compare(tb.LightGreen, g);
		bool _b = light_compare(tb.LightBlue, b);

		// if a lighting check passed, set block lights and push back a new node
		if ((!tb.Solid || tb.Ignored || (tb.XX || tb.XY || tb.YX || tb.YY)) && (_r || _g || _b))
		{
			// set the block light
			TerrainChunk* t = _SetBlockLight(blockPos, chunkPos, _r ? r - 1 : tb.LightRed, _g ? g - 1 : tb.LightGreen, _b ? b - 1 : tb.LightBlue);

			// create a new light creation node
			m_BlockLightCreation.emplace(blockPos, chunkPos, _r ? r - 1 : tb.LightRed, _g ? g - 1 : tb.LightGreen, _b ? b - 1 : tb.LightBlue);
		}
	};

	// loop through the creation list
	while (!m_BlockLightCreation.empty())
	{
		// get the light node details at this point
		LightNode& light = m_BlockLightCreation.front();
		chunkPos = light.chunkPos;
		blockPos = light.blockPos;
		r = light.red;
		g = light.green;
		b = light.blue;
		m_BlockLightCreation.pop();

		// if for whatever reason there's a solid block here, continue
		TerrainBlock block = _GetBlock(blockPos, chunkPos);
		if (block.Solid && !block.Ignored && !(block.XX || block.XY || block.YX || block.YY))
			continue;

		// check lights adjacent to this block
		check_light(chunkPos, blockPos + vector3di(-1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(0, -1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 0, -1));
		check_light(chunkPos, blockPos + vector3di(0, 0, 1));
	}
}

void TerrainManager::UpdateLightDeletion()
{
	// update light deletion
	vector3di chunkPos, blockPos;
	GLuint r, g, b;

	// function to compare light levels
	auto light_compare = [](GLuint v, GLuint c) -> bool {
		return v > 0 && v < c;
	};

	// function to check a light value at a given point
	auto check_light = [&](vector3di chunkPos, vector3di blockPos) {
		// if this block position is out of this chunk, correct it
		PositionCorrect(blockPos, chunkPos);

		// get the block at this position
		TerrainBlock tb = _GetBlock(blockPos, chunkPos);

		// compare lighting values
		bool _r = light_compare(tb.LightRed, r);
		bool _g = light_compare(tb.LightGreen, g);
		bool _b = light_compare(tb.LightBlue, b);

		// if a lighting check passed, set block lights and push back a new node
		if (_r || _g || _b)
		{
			// set the block light
			TerrainChunk* t = _SetBlockLight(blockPos, chunkPos, _r ? 0 : tb.LightRed, _g ? 0 : tb.LightGreen, _b ? 0 : tb.LightBlue);

			// create a new light deletion node
			m_BlockLightDeletion.emplace(blockPos, chunkPos, (int)tb.LightRed, (int)tb.LightGreen, (int)tb.LightBlue);
		}
		else
			m_BlockLightCreation.emplace(blockPos, chunkPos, (int)tb.LightRed, (int)tb.LightGreen, (int)tb.LightBlue);
	};

	// loop through the deletion list
	while (!m_BlockLightDeletion.empty())
	{
		// get the light node details at this point
		LightNode& light = m_BlockLightDeletion.front();
		chunkPos = light.chunkPos;
		blockPos = light.blockPos;
		r = light.red;
		g = light.green;
		b = light.blue;
		m_BlockLightDeletion.pop();

		// check lights adjacent to this block
		check_light(chunkPos, blockPos + vector3di(-1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(0, -1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 0, -1));
		check_light(chunkPos, blockPos + vector3di(0, 0, 1));
	}
}

TerrainChunk* TerrainManager::SetSunLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint val)
{
	// set the sky light at this position and push back a new light creation node
	TerrainChunk* ret = _SetSunLight(blockPos, chunkPos, val);
	m_SunLightCreation.emplace(blockPos, chunkPos, val);

	// update light creation
	UpdateSunlightCreation();

	// return the chunk
	return ret;
}

void TerrainManager::RemoveSunLight(const vector3di& blockPos, const vector3di& chunkPos, const TerrainBlock& block)
{
	// get the block already here
	m_SunLightDeletion.emplace(blockPos, chunkPos, (int)block.SkyLight);

	// set the sky light here
	_SetSunLight(blockPos, chunkPos, 0);

	// update light deletion
	UpdateSunlightDeletion();
}

TerrainChunk* TerrainManager::_SetSunLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint val)
{
	// set the sky/sun light at the given position
	TerrainChunk *pChunk = GetChunk(chunkPos);
	if (pChunk)
		pChunk->terrain[blockPos.x * CHUNK_SIZE * CHUNK_SIZE + blockPos.z * CHUNK_SIZE + blockPos.y].SkyLight = val;

	return pChunk;
}

void TerrainManager::UpdateSunlightCreation()
{
	// update light creation
	vector3di chunkPos, blockPos;
	GLuint val;

	// function to compare light levels
	auto light_compare = [](GLuint v, GLuint c, bool vertical) -> bool {
		if (vertical && c == 15)
			return true;
		else
			return v + 2 <= c;
	};

	// function to check a light value at a given point
	auto check_light = [&](vector3di chunkPos, vector3di blockPos, bool vertical = false) {
		// if this block position is out of this chunk, correct it
		PositionCorrect(blockPos, chunkPos);

		// get the block at this position
		TerrainBlock tb = _GetBlock(blockPos, chunkPos);

		// compare lighting values
		bool _l = light_compare(tb.SkyLight, val, vertical);

		// if we are entering an uncreated chunk, do nothing
		TerrainMap::iterator it = m_Terrain.find(chunkPos);
		if (it == m_Terrain.end() || !it->second || !it->second->bGenerated)
			return;

		// if the light check passed
		if ((!tb.Solid || tb.Ignored || (tb.XX || tb.XY || tb.YX || tb.YY)) && _l)
		{
			// set the sky light
			GLuint l = vertical && val == 15 ? 15 : val - 1;
			TerrainChunk* t = _SetSunLight(blockPos, chunkPos, l);

			// create a new sky light creation node
			m_SunLightCreation.emplace(blockPos, chunkPos, l);
		}
	};

	// loop through the creation list
	while (!m_SunLightCreation.empty())
	{
		// get the light node details at this point
		SunlightNode& light = m_SunLightCreation.front();
		chunkPos = light.chunkPos;
		blockPos = light.blockPos;
		val = light.val;// GetBlock(pos).SkyLight;
		m_SunLightCreation.pop();

		// check lights adjacent to this block
		check_light(chunkPos, blockPos + vector3di(-1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(0, -1, 0), true);
		check_light(chunkPos, blockPos + vector3di(0, 1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 0, -1));
		check_light(chunkPos, blockPos + vector3di(0, 0, 1));
	}
}

void TerrainManager::UpdateSunlightDeletion()
{
	// update light deletion
	vector3di chunkPos, blockPos;
	GLuint val;

	// function to compare light levels
	auto light_compare = [](GLuint v, GLuint c, bool vertical) -> bool {
		if (vertical && c == 15)
			return true;
		else
			return v > 0 && v < c;
	};

	// function to check a light value at a given point
	auto check_light = [&](vector3di chunkPos, vector3di blockPos, bool vertical = false) {
		// if this block position is out of this chunk, correct it
		PositionCorrect(blockPos, chunkPos);

		// get the block at this position
		TerrainBlock tb = _GetBlock(blockPos, chunkPos);

		// compare lighting values
		bool _l = light_compare(tb.SkyLight, val, vertical);

		// if the light check passed
		if ((!tb.Solid || tb.Ignored) && _l)
		{
			// set the sky light
			TerrainChunk* t = _SetSunLight(blockPos, chunkPos, 0);

			// create a new sky light deletion node
			m_SunLightDeletion.emplace(blockPos, chunkPos, 15);
		}
		else if (tb.BlockType == 0)
			m_SunLightCreation.emplace(blockPos, chunkPos, (int)tb.SkyLight);
	};

	// loop through the deletion list
	while (!m_SunLightDeletion.empty())
	{
		// get the light node details at this point
		SunlightNode& light = m_SunLightDeletion.front();
		chunkPos = light.chunkPos;
		blockPos = light.blockPos;
		val = light.val;
		m_SunLightDeletion.pop();

		// check lights adjacent to this block
		check_light(chunkPos, blockPos + vector3di(-1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(0, -1, 0), true);
		check_light(chunkPos, blockPos + vector3di(0, 1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 0, -1));
		check_light(chunkPos, blockPos + vector3di(0, 0, 1));
	}
}

float TerrainManager::GetChunkPercentageSpace(const vector3di& chunkPos)
{
	// figure out wwhat percentage of blocks are space in this chunk
	if (m_Terrain.find(chunkPos) != m_Terrain.end())
	{
		TerrainChunk* chunk = m_Terrain.at(chunkPos);
		float empty = 0;
		static const float ratio = 1.f / (CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);

		for (int i = 0; i < CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE; i++) {
			if (chunk->terrain[i].BlockType == 0)
				empty += ratio;
		}

		return empty;
	}

	// if we couldn't find a chunk here for whatever reason, return 100% empty
	return 1;
}
