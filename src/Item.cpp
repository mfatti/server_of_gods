#include "Item.h"
#include "XMLReader.h"
#include "Terrain.h"

std::map<string, ItemClass*, StringCompare> Item::ListOfItems;
std::vector<ItemClass*> Item::VectorOfItems;
std::vector<ItemClass*> Item::ListOfBlocks;
//std::vector<GLColour> Item::ListOfBlockColours;

static ItemType XMLItemType(string value)
{
	// another function because we can't switch on strings!
	if (value == "pointless")
		return POINTLESS;
	else if (value == "block")
		return BLOCK;
	else if (value == "tool")
		return TOOL;
	else if (value == "weapon")
		return WEAPON;
	else if (value == "armour")
		return ARMOUR;
	else if (value == "misc")
		return MISC;
	else if (value == "ammo")
		return AMMO;
	else if (value == "projectile")
		return PROJECTILE;

	return NO_TYPE;
}

static SubType XMLItemSubType(string value)
{
	// another function because we can't switch on strings!
	if (value == "terrain")
		return TERRAIN;
	else if (value == "terrain_gravity")
		return TERRAIN_GRAVITY;
	else if (value == "tree")
		return TREE;
	else if (value == "wood")
		return WOOD;
	else if (value == "pickaxe")
		return PICKAXE;
	else if (value == "axe")
		return AXE;
	else if (value == "melee")
		return MELEE;
	else if (value == "ranged")
		return RANGED;
	else if (value == "magic")
		return MAGIC;
	else if (value == "summon")
		return SUMMON;
	else if (value == "ingot")
		return INGOT;
	else if (value == "door")
		return DOOR;
	else if (value == "arrow")
		return ARROW;
	else if (value == "bullet")
		return BULLET;
	else if (value == "helmet")
		return HELMET;
	else if (value == "chestpiece")
		return CHESTPIECE;
	else if (value == "gloves")
		return GLOVES;
	else if (value == "greaves")
		return GREAVES;
	else if (value == "boots")
		return BOOTS;

	return NO_SUBTYPE;
}

bool Item::LoadItems(string file, string type)
{
	// create an XML file reader
	XMLReader XML(file);
	if (!XML.IsOpen())
		return false;

	// read the file
	const string strItemsTag("items");
	string strCurrentSection, strNodeName;
	ItemClass *item = NULL;
	bool bLoaded = false;
	//ParticleDefinition p;

	while (XML.ReadLine())
	{
		if (XML.IsNode(strItemsTag))
			strCurrentSection = strItemsTag;
		else if (strCurrentSection == strItemsTag && XML.IsNode(type))
			strCurrentSection = type;
		else if (strCurrentSection == type)
		{
			// check if we have reached the end of the item
			if (XML.IsEndOfNode(strCurrentSection))
			{
				strCurrentSection = strItemsTag;

				// make sure we have a mesh for blocks
				if (item)
				{
					//if (!item->bBlockHasMesh)
					//	item->pMesh = pSceneMan->CreateTerrainBlock(item->strName);

					if (item->Drops.empty())
						item->Drops.push_back(std::pair<string, short>(item->strName, 1));
				}

				continue;
			}

			// look for what we need
			strNodeName = XML.GetNodeName();
			if (strNodeName == "name")
			{
				item = new ItemClass();
				item->strName = XML.GetAttributeValue("value");
				item->Type = XMLItemType(type);

				// make sure this item doesn't already exist
				if (ListOfItems[item->strName] != NULL)
				{
					delete item;
					item = NULL;

					strCurrentSection = strItemsTag;
				}
				else
				{
					ListOfItems[item->strName] = item;
					bLoaded = true; // we have loaded at least one so return true

					// if this is a block type, add it to the list of blocks
					if (type == "block")
					{
						unsigned short n = (unsigned short)ListOfBlocks.size() + 1;
						item->sValues[0] = n;
						ListOfBlocks.push_back(item);
						//ListOfBlockColours.push_back(GLColour(0, 0));
						//std::cout << n << " : " << ListOfBlocks[n].c_str() << std::endl;
					}
				}
			}
			else if (strNodeName == "mesh_path")
			{
				string strMesh = "media/";
				strMesh.append(XML.GetAttributeValue("value"));

				/*if (strMesh.substr(strMesh.length() - 4, 4) == ".obj")
					item->pMesh = pSceneMan->LoadMeshFromOBJ(strMesh);
				else
				{
					item->pMesh = pSceneMan->LoadMeshFromAOBJ(strMesh);
					item->bAnimated = true;
				}*/
				item->bBlockHasMesh = true;
			}
			/*else if (strNodeName == "texture_path")
			{
				string strTexture = "media/";
				strTexture.append(XML.GetAttributeValue("value"));
				item->strTexturePath = strTexture;

				if (type == "block")
					pSceneMan->GetTextureManager()->GetTexture(strTexture);
			}*/
			else if (strNodeName == "subtype")
			{
				item->TypeSub = XMLItemSubType(XML.GetAttributeValue("value"));
				item->sValues[3] = atoi(XML.GetAttributeValue("transparent").c_str());
			}
			/*else if (strNodeName == "texture_coords")
			{
				int x = atoi(XML.GetAttributeValue("x").c_str());
				int y = atoi(XML.GetAttributeValue("y").c_str());
				for (int i = 0; i < 6; i++) {
					ToUV(item->TerrainUV[i], vector2di(x, y));
				}

				if (item->strTexturePath.empty())
					item->strTexturePath = "media/terrain_tiles.png";
			}
			else if (strNodeName == "texture_coords_top")
			{
				int x = atoi(XML.GetAttributeValue("x").c_str());
				int y = atoi(XML.GetAttributeValue("y").c_str());
				ToUV(item->TerrainUV[0], vector2di(x, y));
			}
			else if (strNodeName == "texture_coords_bot")
			{
				int x = atoi(XML.GetAttributeValue("x").c_str());
				int y = atoi(XML.GetAttributeValue("y").c_str());
				ToUV(item->TerrainUV[1], vector2di(x, y));
			}
			else if (strNodeName == "texture_coords_front")
			{
				int x = atoi(XML.GetAttributeValue("x").c_str());
				int y = atoi(XML.GetAttributeValue("y").c_str());
				ToUV(item->TerrainUV[2], vector2di(x, y));
			}
			else if (strNodeName == "texture_coords_back")
			{
				int x = atoi(XML.GetAttributeValue("x").c_str());
				int y = atoi(XML.GetAttributeValue("y").c_str());
				ToUV(item->TerrainUV[3], vector2di(x, y));
			}
			else if (strNodeName == "texture_coords_right")
			{
				int x = atoi(XML.GetAttributeValue("x").c_str());
				int y = atoi(XML.GetAttributeValue("y").c_str());
				ToUV(item->TerrainUV[4], vector2di(x, y));
			}
			else if (strNodeName == "texture_coords_left")
			{
				int x = atoi(XML.GetAttributeValue("x").c_str());
				int y = atoi(XML.GetAttributeValue("y").c_str());
				ToUV(item->TerrainUV[5], vector2di(x, y));
			}*/
			else if (strNodeName == "stack_size")
			{
				item->sStackSize = atoi(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "damage" || strNodeName == "tool_damage" || strNodeName == "defence")
			{
				item->sValues[1] = atoi(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "knockback")
			{
				item->sValues[6] = atoi(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "block_health")
			{
				item->sValues[2] = atoi(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "required_power")
			{
				item->sValues[1] = atoi(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "interactable")
			{
				item->sValues[3] = 1;
			}
			else if (strNodeName == "inventory")
			{
				item->sValues[4] = atoi(XML.GetAttributeValue("size").c_str());
			}
			else if (strNodeName == "block_size")
			{
				int w = atoi(XML.GetAttributeValue("width").c_str());
				int h = atoi(XML.GetAttributeValue("height").c_str());
				int d = atoi(XML.GetAttributeValue("depth").c_str());
				item->BlockSize.Set(w, h, d);
			}
			else if (strNodeName == "description")
			{
				item->strDescription = XML.GetAttributeValue("value");
			}
			else if (strNodeName == "light_colour")
			{
				int r = atoi(XML.GetAttributeValue("red").c_str());
				int g = atoi(XML.GetAttributeValue("green").c_str());
				int b = atoi(XML.GetAttributeValue("blue").c_str());
				item->LightColour.Set(r, g, b);
			}
			else if (strNodeName == "solid")
			{
				item->bSolid = atoi(XML.GetAttributeValue("value").c_str()) > 0;
			}
			else if (strNodeName == "drop")
			{
				string name = XML.GetAttributeValue("item");
				short amount = atoi(XML.GetAttributeValue("amount").c_str());
				if (amount < 1)
					amount = 1;
				item->Drops.push_back(std::pair<string, short>(name, amount));
			}
			else if (strNodeName == "speed")
			{
				item->fValues[0] = (float)atof(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "gravity")
			{
				string value = XML.GetAttributeValue("affected");
				if (value == "true")
					item->sValues[7] = 1;
			}
			else if (strNodeName == "ammo_type")
			{
				string value = XML.GetAttributeValue("value");
				item->AmmoType = XMLItemSubType(value);
			}
			else if (strNodeName == "projectile_type")
			{
				item->strProjectileType = XML.GetAttributeValue("value");
			}
			else if (strNodeName == "mana_cost")
			{
				item->sValues[2] = atoi(XML.GetAttributeValue("value").c_str());
			}
			/*else if (type == "block" && strNodeName == "map_colour")
			{
				ListOfBlockColours[ListOfBlockColours.size() - 1].Set(atoi(XML.GetAttributeValue("red").c_str()),
					atoi(XML.GetAttributeValue("green").c_str()),
					atoi(XML.GetAttributeValue("blue").c_str()), 200);
			}
			else if (XML.IsNode("particle_effect"))
			{
				strCurrentSection = "particle_effect";
				p.anims.clear();
			}*/
		}
		/*else if (strCurrentSection == "particle_effect")
		{
			// check if we have reached the end of the particle definition
			if (XML.IsEndOfNode(strCurrentSection))
			{
				strCurrentSection = type;

				// add the particle to the list
				item->Particles.push_back(p);

				continue;
			}

			// look for what we need
			strNodeName = XML.GetNodeName();
			if (strNodeName == "texture_path")
			{
				string strTexture = "media/";
				strTexture.append(XML.GetAttributeValue("value"));
				p.texture = strTexture;
			}
			else if (strNodeName == "emitter_position")
			{
				float x = atof(XML.GetAttributeValue("x").c_str());
				float y = atof(XML.GetAttributeValue("y").c_str());
				float z = atof(XML.GetAttributeValue("z").c_str());
				p.pos.Set(x, y, z);
			}
			else if (strNodeName == "direction")
			{
				float x = atof(XML.GetAttributeValue("x").c_str());
				float y = atof(XML.GetAttributeValue("y").c_str());
				float z = atof(XML.GetAttributeValue("z").c_str());
				p.dir.Set(x, y, z);
			}
			else if (strNodeName == "speed")
			{
				float speed = atof(XML.GetAttributeValue("speed").c_str());
				p.speed = speed;
			}
			else if (strNodeName == "count")
			{
				int count = atoi(XML.GetAttributeValue("count").c_str());
				p.amount = count;
			}
			else if (strNodeName == "size")
			{
				float x = atof(XML.GetAttributeValue("x").c_str());
				float y = atof(XML.GetAttributeValue("y").c_str());
				p.psize.Set(x, y);
			}
			else if (strNodeName == "spread")
			{
				float spread = atof(XML.GetAttributeValue("spread").c_str());
				p.spread = spread;
			}
			else if (strNodeName == "alpha")
			{
				float a = atof(XML.GetAttributeValue("value").c_str());
				p.alpha = a;
			}
			else if (strNodeName == "frame_size")
			{
				float x = atof(XML.GetAttributeValue("x").c_str());
				float y = atof(XML.GetAttributeValue("y").c_str());
				p.size.Set(x, y);
			}
			else if (strNodeName == "animation")
			{
				vector2di start(atoi(XML.GetAttributeValue("startx").c_str()), atoi(XML.GetAttributeValue("starty").c_str()));
				vector2di end(atoi(XML.GetAttributeValue("endx").c_str()), atoi(XML.GetAttributeValue("endy").c_str()));
				p.anims.push_back(std::pair<vector2di, vector2di>(start, end));
			}
		}*/
	}

	return bLoaded;
}

void Item::SortItems()
{
	// iterate through all the items in the item map and assign them an index in vectorofitems
	// calling this AFTER loading all items will ensure that the order in which xml files are loaded is not important
	std::map<string, ItemClass*, StringCompare>::iterator it = ListOfItems.begin();

	while (it != ListOfItems.end())
	{
		it->second->index = VectorOfItems.size();
		VectorOfItems.push_back(it->second);

		it++;
	}
}

void Item::UnloadItems()
{
	for (std::map<string, ItemClass*>::iterator it = ListOfItems.begin(); it != ListOfItems.end(); ++it) {
		delete it->second;
	}
	ListOfItems.clear();
	VectorOfItems.clear();
	ListOfBlocks.clear();
}

bool Item::ReloadItems(string file, string type)
{
	UnloadItems();
	return LoadItems(file, type);
}

const ItemClass* Item::GetItemClass(string name)
{
	return ListOfItems[name];
}

const ItemClass* Item::GetItemClass(unsigned int n)
{
	return VectorOfItems[n];
}

const ItemClass* Item::GetItemClass(const TerrainBlock& n)
{
	if (n.BlockType == 0)
		return NULL;

	return ListOfBlocks[n.BlockType - 1];
}

const GLuint Item::GetBlockFromName(string name)
{
	return ListOfItems[name]->sValues[0];
}

/*const GLColour Item::GetBlockMapColour(TerrainBlock b)
{
	if (b.BlockType == 0)
		return GLColour();

	return ListOfBlockColours[b.BlockType - 1];
}*/

Item::Item(string type, short amount, vector3df pos)
	: m_sCurrentStack(amount)
{
	m_ItemClass = ListOfItems[type];

	/*SetMesh(m_ItemClass.pMesh);
	SetTexture(m_ItemClass.strTexturePath.c_str());

	// set the scale
	if (m_ItemClass.Type == BLOCK)
		SetScale(vector3df(1 / (2.5f * m_ItemClass.BlockSize.x), 1 / (2.5f * m_ItemClass.BlockSize.y), 1 / (2.5f * m_ItemClass.BlockSize.z)));

	// get a pointer to the player object
	m_pPlayer = GetSceneManager()->GetFirstInstance<Player>();*/
}

Item::Item(unsigned int type, short amount, vector3df pos)
	: m_sCurrentStack(amount)
{
	m_ItemClass = VectorOfItems[type];
}

Item::Item(Item* other, bool visible)
{
	//SetVisible(visible);

	m_ItemClass = ListOfItems[other->m_ItemClass.strName];
	m_sCurrentStack = other->m_sCurrentStack;

	/*SetMesh(m_ItemClass.pMesh);
	SetTexture(m_ItemClass.strTexturePath.c_str());

	// set the scale
	if (m_ItemClass.Type == BLOCK)
		SetScale(vector3df(1 / (2.5f * m_ItemClass.BlockSize.x), 1 / (2.5f * m_ItemClass.BlockSize.y), 1 / (2.5f * m_ItemClass.BlockSize.z)));*/
}

bool Item::SetStackSize(short size)
{
	if (size == -1)
	{
		m_sCurrentStack = m_ItemClass.sStackSize;
		return true;
	}

	if (size <= m_ItemClass.sStackSize)
	{
		m_sCurrentStack = size;
		return true;
	}

	return false;
}

bool Item::IncreaseStackSize(short amount)
{
	if (m_sCurrentStack + amount <= m_ItemClass.sStackSize)
	{
		m_sCurrentStack += amount;
		return true;
	}

	return false;
}

bool Item::DecreaseStackSize(short amount)
{
	if (m_sCurrentStack - amount > 0)
	{
		m_sCurrentStack -= amount;
		return true;
	}

	return false;
}

short Item::GetStackSize()
{
	return m_sCurrentStack;
}

short Item::GetMaxStackSize()
{
	return m_ItemClass.sStackSize;
}

short Item::GetDamage()
{
	return m_ItemClass.sValues[1];
}

unsigned short Item::GetBlockType()
{
	return m_ItemClass.sValues[0];
}

bool Item::operator==(Item& other)
{
	return (m_ItemClass.strName == other.m_ItemClass.strName);
}

bool Item::operator==(const ItemClass& other)
{
	return (m_ItemClass.strName == other.strName);
}

string Item::GetName()
{
	return m_ItemClass.strName;
}

string Item::GetDescription()
{
	return m_ItemClass.strDescription;
}

unsigned int Item::GetIndex()
{
	return m_ItemClass.index;
}

void Item::WriteTo(vector<unsigned char>& out)
{
	// write this item to the given array, used for saving
	const char* name = m_ItemClass.strName.c_str();
	short length = m_ItemClass.strName.length();

	union {
		short size;
		unsigned char byte[2];
	} conv;
	conv.size = length;

	// output size of this item
	out.push_back(conv.byte[0]);
	out.push_back(conv.byte[1]);

	// output name of the item
	for (short i = 0; i < length; i++) {
		out.push_back((unsigned char)name[i]);
	}

	// output the stack size
	conv.size = m_sCurrentStack;
	out.push_back(conv.byte[0]);
	out.push_back(conv.byte[1]);
}

/*void Item::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;

	// if we are in the menu, do nothing
	if (!bForce && IsMenuOpen())
		return;

	// if we are near the player then make him pick up this item
	if (m_pPlayer)
	{
		GLfloat dist = GetPosition().Distance(m_pPlayer->GetPosition());
		if (dist < 20.0f)
			m_pPlayer->PickUpItem(this);
	}

	// call GLObject::Update
	UpdateTransformationMatrix();
}*/